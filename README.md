# Git basics:
## Baixar:
git pull origin master

## Pujar:
git push origin master

## Crear Versió (tag):
git tag -a v1.1 -m 'version 1.1'

git push origin v1.1

## Canviar de branca
git checkout branca

## Pujar canvis fets en una branca
git push origin branca

## Merge branca a la branca actual
git merge branca

## Important
No tocar master(excepte propietari)

## Problemes:
http://rogerdudler.github.io/git-guide/

