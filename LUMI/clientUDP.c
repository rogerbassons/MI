//Roger Bassons Renart & Miquel Farreras Casamort

//CLIENT UDP

#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <stdlib.h>
#include <string.h>
#include <netdb.h>

int main()
{
	printf("Entra el nom DNS:\n");
	char nomDNS[50];
	scanf("%s",nomDNS);

	struct hostent *dadesHOST;
	struct in_addr adrHOST;
	dadesHOST = gethostbyname(nomDNS);
	adrHOST.s_addr = *((unsigned long *)dadesHOST->h_addr_list[0]);
	char ipserv[50];
	strcpy(ipserv,(char*)inet_ntoa(adrHOST));
	printf("Entra el Port:\n");
	int port;
	scanf("%d", &port);
	
	int s;
	if((s = socket(AF_INET, SOCK_DGRAM, 0)) == -1) { 
		perror("Error en socket\n"); 
		exit(-1);   
	}

	struct sockaddr_in adrr;         /* adreça del socket remot del servidor per connect */ 
	
	adrr.sin_family= AF_INET; 
	adrr.sin_addr.s_addr= inet_addr(ipserv); 
	adrr.sin_port = htons(port);
	int i;
	for(i=0;i<8;i++){adrr.sin_zero[i]='0';}
	
	unsigned int long_adrr = sizeof(adrr); 
	
	char buff[200];
	char c;
	while((c = getchar()) != '\n' && c != EOF); // eliminar el \n que ha quedat al buffer despres de scanf
	do {
		printf("Missatge:\n");
		fgets(buff, 200, stdin);
		buff[strlen(buff)-1] = '\0';
		sendto(s, buff, strlen(buff), 0, (struct sockaddr*) &adrr, long_adrr);

		int n = recvfrom(s, buff, 200, 0, (struct sockaddr *) &adrr, &long_adrr);
		buff[n] = '\0';
		printf("%s\n",buff);
	} while (strcmp(buff, "FI") != 0);
	
	close(s);
	return 0;
}	
