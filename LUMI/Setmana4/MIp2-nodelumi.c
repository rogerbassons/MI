/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer nodelumi.c que implementa la interfície aplicació-administrador */
/* d'un node de LUMI, sobre la capa d'aplicació de LUMI (fent crides a la */
/* interfície de la capa LUMI -fitxers lumi.c i lumi.h-).                 */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp2-lumi.h" per poder fer crides a la interfície de LUMI     */
#include "MIp2-lumi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Definició de constants, p.e., #define MAX_LINIA 150                    */
#define PORT_NODE 60000
#define N_USUARIS 10

//Si troba dades a nomsUsuari[i] retorna i, altrament retorna N_USUARIS

int buscarUsuari(const char * dades, char nomsUsuari[N_USUARIS][300]) {
	int i = 0;
	while (strcmp(dades, nomsUsuari[i]) != 0 && i<N_USUARIS) {
		i++;
	}
	return i;
}

int main(int argc,char *argv[])
{
	/* Declaració de variables, p.e., int n;                                 */
	char nomsUsuari[N_USUARIS][300];
	char ipsUsuaris[N_USUARIS][16];
	int portsUsuaris[N_USUARIS];
	int i;
	char buff[300], nomDomini[300];
	int idFitxer;
	char tipusPet;
	char dades[300];
	char IPrem[16];
	int portUDPrem;
	/* Expressions, estructures de control, crides a funcions, etc.          */

	FILE *fp = fopen("MIp2-nodelumi.cfg", "r");
	fscanf(fp, "%s", buff);
	strcpy(nomDomini, buff);
	for (i=0; i < N_USUARIS; i++) {
		fscanf(fp, "%s", buff);
		strcpy(nomsUsuari[i], buff);
		strcpy(ipsUsuaris[i], "0");
		portsUsuaris[i] = 0;
	}
	fclose(fp);

	int sck = LUMI_IniciaEscoltaPeticionsRemotes("0.0.0.0", 60000, "logNode", &idFitxer);
	while (1) {
		tipusPet = 'N';
		portUDPrem = 0;
		if (LUMI_HaArribatPeticio(sck) != sck) {
			perror("Error al esperar peticio");
			return -1;
		} else {
			LUMI_RebrePeticio(sck, tipusPet, dades, IPrem, &portUDPrem, idFitxer);
		}
		i = 0;
		if (tipusPet == 'P' || tipusPet == 'D') {
			printf("%s",dades);

			i = buscarUsuari(dades, nomsUsuari);
			if (i < N_USUARIS) {
				if (tipusPet == 'P' && portsUsuaris[i] == 0) {
					strcpy(ipsUsuaris[i], IPrem);
					portsUsuaris[i] = portUDPrem;
					LUMI_EnviarConfirmacio(sck, "0", IPrem, &portUDPrem, idFitxer); //OK
				}
				if (tipusPet == 'D' && portsUsuaris[i] != 0) {
					strcpy(ipsUsuaris[i], "0");
					portsUsuaris[i] = 0;
					LUMI_EnviarConfirmacio(sck, "0", IPrem, &portUDPrem, idFitxer); //OK
				}
				else {
					LUMI_EnviarConfirmacio(sck, "2", IPrem, &portUDPrem, idFitxer); //Usuari ja registrat/no registrat
				}
			}
			else {
				LUMI_EnviarConfirmacio(sck, "1", IPrem, &portUDPrem, idFitxer); //Usuari no existeix
			}
		}
		
		/* cal mirar si s'ha desregistrat algú amb timeout */
	}
	LUMI_FinalitzaEscoltaPeticionsRemotes(sck, idFitxer);
}
