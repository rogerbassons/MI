/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer p2p.c que implementa la interfície aplicació-usuari de          */
/* l'aplicació de MI amb l'agent de LUMI integrat, sobre les capes        */
/* d'aplicació de MI i LUMI (fent crides a les interfícies de les capes   */
/* MI -fitxers mi.c i mi.h- i LUMI -lumi.c i lumi.h- ).                   */
/* Autors: Roger Bassons, Miquel Farreras                                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp2-mi.h" per poder fer crides a la interfície de MI i       */
/* "MIp2-lumi.h" per poder fer crides a la interfície de LUMI.            */
#include "MIp2-mi.h"
#include "MIp2-lumi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Definició de constants, p.e., #define MAX_LINIA 150                    */
#define portDnsNode 60000;

/* Amb "username"@"domain"  a nomUsuLoc, deixa el username a nomUsuLoc i el nom DNS a dnsNode */
/* tots dos acaben en "\0" */
void separarUsernameDomain(char * nomUsuLoc, char * dnsNode) {
	int i = 0;
	while (nomUsuLoc[i] != '@') {
		i++;
	}
	nomUsuLoc[i] = '\0';
	i++;
	int j = 0;
	while (nomUsuLoc[i] != '\0') {
		dnsNode[j] = nomUsuLoc[i];
		i++;
		j++;
	}
	dnsNode[j] = '\0';
}

int main(int argc,char *argv[])
{
	/* Declaració de variables, p.e., int n;                                 */
	int port;
	char nomUsuLoc[300], nomUsuRem[300], NicLoc[300], NicRem[300], dnsNode[300], fitxerLog[300];
	char IPloc[16], IPrem[16], IPlocClientNode[16];
	int portTCPloc, portTCPrem;
	char buff[300];
	int n;
	int idFitxer=0;
	int sck, sConnectat, sPeticio, sServidor;
	int acabar;

	/* Expressions, estructures de control, crides a funcions, etc.          */
	printf("Entra el port TCP del servidor, si vols que s'assigni automàticament entra 0\n");
	scanf("%d", &port);

	if ((sPeticio = MI_IniciaEscPetiRemConv(port)) == -1) {
		return -1;
	}

	if (MI_TrobaPortEscoltaLocal(sPeticio, &port) == -1) {
		return -1;
	}
	
	printf("El port TCP del servidor es: %i\n", port);
	printf("Llistat d'Interficies amb la seva IP escoltant:\n");
	MI_MostrarIPs();
	
	printf("Entra la ip per on vols crear el socket per contactar amb el node:\n");
	n = read(0,IPlocClientNode,16);
	IPlocClientNode[n-1] = '\0';
	
	printf("Entra el teu username@domain:\n");
	n = read(0, nomUsuLoc,300);
	nomUsuLoc[n-1] = '\0';
	
	separarUsernameDomain(nomUsuLoc, dnsNode);
	
	printf("Entra el nom del fitxer de log:\n");
	n = read(0, fitxerLog,300);
	fitxerLog[n-1] = '\0';
	
	if ((sServidor=LUMI_InicialitzaClient(IPlocClientNode, fitxerLog, idFitxer)) == -1) {
		perror("Error al inicialitzar el client");
		return -1;
	}
	
	if (LUMI_registrar(sServidor, nomUsuLoc, dnsNode) != 1) {
		perror("Error al registrar");
		return -1;
	}

	printf("Entra el teu Nickname:\n");
	n = read(0,NicLoc,300);
	NicLoc[n-1] = '\0';

	acabar = 0;

	do {
		printf("Entra l'adreça de l'usuari remot per a connectar-te, FI per acabar, altrament espera\n");

		if ((sck = MI_HaArribatPetiConv(sPeticio)) == -1) {
			return -1;
		}

		if (sck == 0) {
			n = read(0,nomUsuRem,300);
			nomUsuRem[n-1] = '\0';
			if (strcmp(nomUsuRem,"FI") == 0) {
				acabar = 1;
			}
			else {
				if (LUMI_localitzar(sPeticio, nomUsuRem, IPrem, &portTCPrem) == -1) {
					return -1;
				}

				if((sConnectat = MI_DemanaConv(IPrem, portTCPrem, IPloc, &portTCPloc, NicLoc, NicRem)) == -1) {
					return -1;
				}
			}

		} else if (sck == sPeticio) {
			if((sConnectat = MI_AcceptaConv(sPeticio, IPrem, &portTCPrem, IPloc, &portTCPloc, NicLoc, NicRem)) == -1) {
				return -1;
			}
		}

		if (acabar == 0) {
			printf("El teu Nickname: %s\n", NicLoc);
			printf("Adreça IP local: %s\n", IPloc);
			printf("Port TCP local: %i\n", portTCPloc);
			printf("El Nickname remot: %s\n", NicRem);
			printf("Adreça IP remota: %s\n", IPrem);
			printf("Port TCP remot: %i\n", portTCPrem);

			printf("Escriu els missatges:\n");
			do {
				sck = MI_HaArribatLinia(sConnectat);
				if (sck == -1) {
					return -1;
				} else if (sck == 0) {
					n = read(0,buff, 300);
					buff[n-1] = '\0';
					if (strcmp(buff,"FI") != 0) {
						if (MI_EnviaLinia(sConnectat, buff) == -1) {
							printf("No s'ha pogut enviar la linia");
						}
					} else {
						n = -2;
					}
				} else if (sck == sConnectat) {
					if ((n = MI_RepLinia(sConnectat, buff)) != -2) {
						printf("%s diu: %s\n",NicRem,buff);
					}
				}

			} while (n != -2);
			MI_AcabaConv(sConnectat);
			printf("Fi de la comunicació\n");
		}
	} while (acabar == 0);

	if (MI_AcabaEscPetiRemConv(sPeticio) == -1) {
		return -1;
	}
	
	if (LUMI_desregistrar(sPeticio, nomUsuLoc, dnsNode) != 1) {
		perror("Error al desregistrar");
		return -1;
	}
	
	printf("Fi de l'aplicació\n");
	return 0;
	
 }
