/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer agelumi.c que implementa la interfície aplicació-usuari d'un    */
/* agent d'usuari de LUMI sol, no integrat a l'aplicació de MI, sobre la  */
/* capa d'aplicació de LUMI (fent crides a la interfície de la capa LUMI  */
/* -fitxers lumi.c i lumi.h- ).                                           */
/* Autors: X, Y                                                           */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp2-lumi.h" per poder fer crides a la interfície de LUMI     */
#include "MIp2-lumi.h"
#include <stdio.h>
#include <unistd.h>

/* Definició de constants, p.e., #define MAX_LINIA 150                    */


int main(int argc,char *argv[])
{
 /* Declaració de variables, p.e., int n;                                 */
	int idFitxer;
	char adrecaMI[300];
	int Sck;
	int n;
 /* Expressions, estructures de control, crides a funcions, etc.          */

	Sck = LUMI_IniciaEscoltaPeticionsRemotes("0.0.0.0", 0, "agent", &idFitxer);

	printf("Entra el teu usuari de MI:\n");
	n = read(0, adrecaMI,300);
	adrecaMI[n-1] = '\0';

	if (LUMI_Registrar(Sck, adrecaMI, idFitxer) != 1) {
		printf("No s'ha pogut registrar\n");
	}
	else {
		printf("Registrat\n");
	}

	LUMI_FinalitzaEscoltaPeticionsRemotes(Sck, idFitxer);
	return 0;
}
