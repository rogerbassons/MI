/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer lumi.c que implementa la capa d'aplicació de LUMI, sobre la     */
/* de transport UDP (fent crides a la interfície de la capa UDP           */
/* -sockets-).                                                            */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <sys/types.h> o #include "meu.h" */
/*  (si les funcions externes es cridessin entre elles, faria falta fer   */
/*   un #include "MIp2-lumi.h")                                           */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>        
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>



/* Definició de constants, p.e., #define MAX_LINIA 150                    */

#define PORT_NODE 60000
#define MAX_LINIA 300

/* Declaració de funcions internes que es fan servir en aquest fitxer     */
/* (les seves definicions es troben més avall) per així fer-les conegudes */
/* des d'aqui fins al final de fitxer.                                    */
/* Com a mínim heu de fer les següents funcions internes:                 */

int UDP_CreaSock(const char *IPloc, int portUDPloc);
int UDP_EnviaA(int Sck, const char *IPrem, int portUDPrem, const char *SeqBytes, int LongSeqBytes);
int UDP_RepDe(int Sck, char *IPrem, int *portUDPrem, char *SeqBytes, int LongSeqBytes);
int UDP_TancaSock(int Sck);
int UDP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portUDPloc);
int HaArribatAlgunaCosaEnTemps(const int *LlistaSck, int LongLlistaSck, int Temps);
int ResolDNSaIP(const char *NomDNS, char *IP);
int Log_CreaFitx(const char *NomFitxLog);
int Log_Escriu(int FitxLog, const char *MissLog);
int Log_TancaFitx(int FitxLog);

int LOG_enviatMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n);
int LOG_rebutMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n);
int crearLinia(char * dest, char * tipus, const char * missatge);
void copiarInformacio(char * dest, char * src, int n);
void separarNomDomini(const char * adrecaMI, char * domini, char * nom);
int obtenirIPPortTCP(char * buff, int n, char * ip, int * port);

/* Definicio de funcions EXTERNES, és a dir, d'aquelles que en altres     */
/* fitxers externs es faran servir.                                       */
/* En termes de capes de l'aplicació, aquest conjunt de funcions externes */
/* formen la interfície de la capa LUMI.                                  */
/* Les funcions externes les heu de dissenyar vosaltres...                */

/* Descripció del que fa la funció...                                     */
/* Descripció dels arguments de la funció, què son, tipus, si es passen   */
/* per valor o per referència (la funció els omple)...                    */
/* Descripció dels valors de retorn de la funció...                       */

// Inicia l’escolta de peticions remotes de LUMI a través d’un nou
// socket UDP en el #port portUDPloc i a l'@IP IPloc. Si IPloc és "0.0.0.0" llavors
// la @IP del socket és qualsevol @IP local. Si portUDPloc és 0 llavors el #port
// s'assigna automàticament (pot ser qualsevol numero de port lliure)
// Obre un fitxer amb el nom fitxerLog i s'emplena idFitxer amb el seu identificador
// Retorna -1 si hi ha error, altrament retorna  l’identificador del socket d’escolta de LUMI creat si tot va bé
int LUMI_IniciaEscoltaPeticionsRemotes(const char *IPloc, int portUDPloc, const char * fitxerLog, int *idFitxer) {
	*idFitxer = Log_CreaFitx(fitxerLog);
	if (*idFitxer != 1) {
		return UDP_CreaSock(IPloc, portUDPloc);
	}
	else {
		return -1;
	}
}

// adrecaMI és l'adreça de l'usuari de MI que es vol registrar amb format: nom@domini
// Registra, mitjançant socket, l'usuari nom al node LUMI amb del domini domini i port PORT_NODE
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_Registrar(int socket, const char * adrecaMI, int idFitxer) {
	char ipnode[16];
	char iploc[16];
	int portudploc;
	char nomUsuariMI[MAX_LINIA];
	char domini[MAX_LINIA];
	separarNomDomini(adrecaMI, domini, nomUsuariMI);
	
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);

	if (ResolDNSaIP(domini, ipnode) == -1) {
		perror("Error en resoldre DNS a IP");
		return -1;
	}

	char missatgeEnviat[MAX_LINIA];
	int n = crearLinia(missatgeEnviat,"R",nomUsuariMI);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades

		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de registre
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			return -1;
		} else if (resultat != -2) {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer,iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}
	if (rebut && m == 2 && missatgeRebut[0] == 'C') {
		if (missatgeRebut[1] == '0') {
			return 1;
		}
		else if (missatgeRebut[1] == '1') {
			perror("L'usuari no existeix");
			return -1;
		}
		else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI"); // el missatge enviat no compleix el format PLUMI
			return -1;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// adrecaMI és l'adreça de l'usuari de MI que es vol desregistrar amb format: nom@domini
// Desregistra, mitjançant socket, l'usuari nom al node LUMI amb del domini domini i port PORT_NODE
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_Desregistrar(int socket, const char * adrecaMI, int idFitxer)
{
	char ipnode[16];
	char iploc[16];
	int portudploc;
	char nomUsuariMI[MAX_LINIA];
	char domini[MAX_LINIA];
	separarNomDomini(adrecaMI, domini, nomUsuariMI);
	
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);
	
	if (ResolDNSaIP(domini, ipnode) == -1) {
		return -1;
	}
	
	char missatgeEnviat[MAX_LINIA];
	int n = crearLinia(missatgeEnviat,"D",nomUsuariMI);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades
		
		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de registre
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			return -1;
		} else if (resultat != -2) {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer, iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}
	if (rebut && m == 2 && missatgeRebut[0] == 'C') {
		if (missatgeRebut[1] == '0') {
			return 1;
		}
		else if (missatgeRebut[1] == '1') {
			perror("L'usuari no existeix");
			return -1;
		}
		else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI"); // el missatge enviat no compleix el format PLUMI
			return -1;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// adrecaMIloc és l'adreça de l'usuari local amb format: nom1@domini1
// adrecaMIrem és l'adreça de l'usuari de MI que es vol localitzar amb format: nom2@domini2
// Localitza, mitjançant socket, l'usuari nom2 del domini domini2, a traves del node LUMI amb del domini domini1 i port PORT_NODE
// Emplena ip i port amb l'adreça ip i el port de l'usuari de MI amb l'adreça adrecaMIrem.
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_Localitzar(int socket, const char *adrecaMIloc, const char *adrecaMIrem , char * ip, int *port, int idFitxer) 
{
	return 1;
}

// S'espera indefinidament fins que arriba alguna cosa a socket
// Si ha arribat alguna cosa a socket retorna socket, altrament retorna -1
int LUMI_HaArribatPeticio(int socket)
{
	int LlistaSck[1], LongLlistaSck;
	LlistaSck[0] = socket;
	LongLlistaSck = 1;
	return HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  -1);
}
// Tanca el socket UDP sck i el fitxer de log amb identificador idFitxer
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_FinalitzaEscoltaPeticionsRemotes(int Sck, int idFitxer) {
	if (UDP_TancaSock(Sck) == -1) {
		return -1;
	}
	if (Log_TancaFitx(idFitxer) == -1) {
		return -1;
	}
	return 1;
}

// Emplena tipusPet amb el tipus de petició i dades amb la resta de dades de la petició
// Retorna un nombre positiu qualsevol si tot va bé, altrament retorna -1
int LUMI_RebrePeticio(int socketLoc, char tipusPet, char * dades, char * IPrem, int * portUDPrem, int idFitxer) {
	char iploc[16];
	int portudploc;
	int n = 0;
	if (UDP_RepDe(socketLoc, IPrem, portUDPrem, dades, n) == -1) {
		return -1;
	} else {
		UDP_TrobaAdrSockLoc(socketLoc, iploc, &portudploc);
		LOG_rebutMissatge(idFitxer, iploc, portudploc, IPrem, *portUDPrem, dades, n);
		tipusPet = dades[0];
		copiarInformacio(dades, dades, n);
		return n;
	}
}

//Envia a través del socket PORT NODE al socket socketRem de #portUDP portUDPrem i @IP IPrem "C" + el contingut de dades
//Retorna un positiu si tot va bé, -1 si hi ha algun error
int LUMI_EnviarConfirmacio(int socketLoc, int socketRem, char * dades, char * IPrem, int * portUDPrem, int idFitxer) {
	int n;
	char dest[300];
	char iploc[16];
	int portudploc;
	n = crearLinia(dest, "C", dades);
	if (UDP_EnviaA(socketLoc, IPrem, *portUDPrem, dest, n) == -1) {
		return -1;
	} else {
		UDP_TrobaAdrSockLoc(socketLoc, iploc, &portudploc);
		LOG_enviatMissatge(idFitxer, iploc, portudploc, IPrem, *portUDPrem, dest, n);
		return n;
	}
}

/* Definicio de funcions INTERNES, és a dir, d'aquelles que es faran      */
/* servir només en aquest mateix fitxer.                                  */

/* Crea un socket UDP a l’@IP “IPloc” i #port UDP “portUDPloc”            */
/* (si “IPloc” és “0.0.0.0” i/o “portUDPloc” és 0 es fa/farà una          */
/* assignació implícita de l’@IP i/o del #port UDP, respectivament).      */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int UDP_CreaSock(const char *IPloc, int portUDPloc)
{
	int socketUDP;
	if((socketUDP = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		return -1;
	}

	struct sockaddr_in adr;
	adr.sin_family= AF_INET;
	adr.sin_addr.s_addr= inet_addr(IPloc);
	adr.sin_port = htons(portUDPloc);
	int i;
	for(i=0;i<8;i++) {
		adr.sin_zero[i]='0';
	}

	if(bind(socketUDP, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(socketUDP);
		return -1;
	}
	
	return socketUDP;
}

/* Envia a través del socket UDP d’identificador “Sck” la seqüència de    */
/* bytes escrita a “SeqBytes” (de longitud “LongSeqBytes” bytes) cap al   */
/* socket remot que té @IP “IPrem” i #port UDP “portUDPrem”.              */
/* "IPrem" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* "SeqBytes" és un vector de chars qualsevol (recordeu que en C, un      */
/* char és un enter de 8 bits) d'una longitud >= LongSeqBytes bytes       */
/* Retorna -1 si hi ha error; el nombre de bytes enviats si tot va bé.    */
int UDP_EnviaA(int Sck, const char *IPrem, int portUDPrem, const char *SeqBytes, int LongSeqBytes)
{
	struct sockaddr_in adrrem;
	adrrem.sin_family= AF_INET; 
	adrrem.sin_addr.s_addr= inet_addr(IPrem); 
	adrrem.sin_port = htons(portUDPrem);
	int i;
	for(i=0;i<8;i++){adrrem.sin_zero[i]='0';}
	socklen_t long_adrrem = sizeof(adrrem);
	
	int bytes = sendto(Sck, SeqBytes, LongSeqBytes, 0, (struct sockaddr*) &adrrem, long_adrrem);
	if (bytes == -1) {
		perror("Error a sendto");
		return -1;
	}
	
	return bytes;
}

/* Rep a través del socket UDP d’identificador “Sck” una seqüència de     */
/* bytes que prové d'un socket remot i l’escriu a “SeqBytes*” (que té     */
/* una longitud de “LongSeqBytes” bytes).                                 */
/* Omple "IPrem*" i "portUDPrem*" amb respectivament, l'@IP i el #port    */
/* UDP del socket remot.                                                  */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* "SeqBytes*" és un vector de chars qualsevol (recordeu que en C, un     */
/* char és un enter de 8 bits) d'una longitud <= LongSeqBytes bytes       */
/* Retorna -1 si hi ha error; el nombre de bytes rebuts si tot va bé.     */
int UDP_RepDe(int Sck, char *IPrem, int *portUDPrem, char *SeqBytes, int LongSeqBytes)
{
	struct sockaddr_in adrrem;
	socklen_t long_adrrem = sizeof(adrrem);
		
	int bytes = recvfrom(Sck, SeqBytes, LongSeqBytes, 0, (struct sockaddr *) &adrrem, &long_adrrem);
	if (bytes == -1) {
		perror("Error a recvfrom");
		return -1;
	}
	strcpy(IPrem, inet_ntoa(adrrem.sin_addr));
	*portUDPrem = ntohs(adrrem.sin_port);
	return bytes;
}

/* S’allibera (s’esborra) el socket UDP d’identificador “Sck”.            */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int UDP_TancaSock(int Sck)
{
	return close(Sck);
}

/* Donat el socket UDP d’identificador “Sck”, troba l’adreça d’aquest     */
/* socket, omplint “IPloc*” i “portUDPloc*” amb respectivament, la seva   */
/* @IP i #port UDP.                                                       */
/* "IPloc*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int UDP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portUDPloc)
{
	struct sockaddr_in adrl;
	socklen_t long_adrl = sizeof(adrl);
	if (getsockname(Sck, (struct sockaddr *)&adrl, &long_adrl) == -1) {
		perror("Error en getsockname");
		return -1;
	}
	strcpy(IPloc, inet_ntoa(adrl.sin_addr));
	*portUDPloc = ntohs(adrl.sin_port);
	return 1;
}

/* Examina simultàniament durant "Temps" (en [ms] els sockets (poden ser  */
/* TCP, UDP i stdin) amb identificadors en la llista “LlistaSck” (de      */
/* longitud “LongLlistaSck” sockets) per saber si hi ha arribat alguna    */
/* cosa per ser llegida. Si Temps és -1, s'espera indefinidament fins que */
/* arribi alguna cosa.                                                    */
/* "LlistaSck" és un vector d'enters d'una longitud >= LongLlistaSck      */
/* Retorna -1 si hi ha error; retorna -2 si passa "Temps" sense que       */
/* arribi res; si arriba alguna cosa per algun dels sockets, retorna      */
/* l’identificador d’aquest socket.                                       */
int HaArribatAlgunaCosaEnTemps(const int *LlistaSck, int LongLlistaSck, int Temps)
{
	fd_set conjunt;
	FD_ZERO(&conjunt);

	struct timeval timeout;
	double segons = 0;
	if (Temps != -1) {
		segons = Temps / 1000;
	}
	
	int i;
	int descmax = 0;
	for (i = 0; i<LongLlistaSck; i++) {
		FD_SET(LlistaSck[i], &conjunt);
		if (descmax < LlistaSck[i])
			descmax = LlistaSck[i];
	}

	if (Temps != -1) {
		timeout.tv_sec = segons;
		timeout.tv_usec = 0;
		int res = select(descmax+1, &conjunt, NULL, NULL, &timeout);
		if (res == -1) {
			perror("Error en select");
			return -1;
		} else if (res == 0) {
			return -2;
		}
	} else {
		if(select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
			perror("Error en select");
			return -1;
		}
	}
	i = 0;
	while (i<LongLlistaSck && FD_ISSET(LlistaSck[i], &conjunt)==0)
		i++;

	if (i<LongLlistaSck) {
		return LlistaSck[i];
	} else {
		return -1;
	}
}

/* Donat el nom DNS "NomDNS" obté la corresponent @IP i l'escriu a "IP*"  */
/* "NomDNS" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud qualsevol, i "IP*" és un "string" de C (vector de */
/* chars imprimibles acabat en '\0') d'una longitud màxima de 16 chars    */
/* (incloent '\0').                                                       */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé     */
int ResolDNSaIP(const char *NomDNS, char *IP)
{
	struct hostent *dadesHOST;
	struct in_addr adrHOST;
	dadesHOST = gethostbyname(NomDNS);
	if (dadesHOST == NULL) {
		perror("No s'ha pogut resoldre el DNS");
		return -1;
	}
	adrHOST.s_addr = *((unsigned long *)dadesHOST->h_addr_list[0]);
	strcpy(IP,(char*)inet_ntoa(adrHOST));
	return 1;
}

/* Crea un fitxer de "log" de nom "NomFitxLog".                           */
/* "NomFitxLog" és un "string" de C (vector de chars imprimibles acabat   */
/* en '\0') d'una longitud qualsevol.                                     */
/* Retorna -1 si hi ha error; l'identificador del fitxer creat si tot va  */
/* bé.                                                                    */
int Log_CreaFitx(const char *NomFitxLog)
{
	int f = open(NomFitxLog, O_WRONLY | O_CREAT | O_APPEND, 0666);
	if (f == -1) {
		perror("Error en obrir el fitxer");
		return -1;
	}
	return f;
}

/* Escriu al fitxer de "log" d'identificador "FitxLog" el missatge de     */
/* "log" "MissLog".                                                       */
/* "MissLog" és un "string" de C (vector de chars imprimibles acabat      */
/* en '\0') d'una longitud qualsevol.                                     */
/* Retorna -1 si hi ha error; el nombre de caràcters del missatge de      */
/* "log" (sense el '\0') si tot va bé                                     */
int Log_Escriu(int FitxLog, const char *MissLog)
{
	size_t llargada = strlen(MissLog);
	if (write(FitxLog, MissLog, llargada) == -1) {
		perror("Error en escriure al fitxer");
		return -1;
	}
	return llargada;
}

/* Tanca el fitxer de "log" d'identificador "FitxLog".                    */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int Log_TancaFitx(int FitxLog)
{
	return close(FitxLog);
}

// escriu al fitxer amb fd FitxLog la següent línia:
// "IPloc:portUDPloc ha enviat un missatge a IPrem:portUDPrem -> missatge(n)
// retorna 1 si tot va bé
int LOG_enviatMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n) {
	char buff[MAX_LINIA+100];
	char enter[33];
	char buffmissatge[MAX_LINIA];
	strncpy(buffmissatge, missatge, n);
	buff[0] = '\0';
	strcat(buff,IPloc);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPloc);
	strcat(buff, enter);
	strcat(buff," ha enviat un missatge a ");
	strcat(buff,IPrem);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPrem);
	strcat(buff, enter);
	strcat(buff, " -> ");
	strcat(buff, buffmissatge);
	strcat(buff, "(");
	sprintf(enter,"%d",n);
	strcat(buff, enter);
	strcat(buff, ")\n\0");
	return Log_Escriu(FitxLog,buff);
}
// escriu al fitxer amb fd FitxLog la següent línia:
// "IPloc:portUDPloc ha rebut un missatge de IPrem:portUDPrem -> missatge(n)
// retorna 1 si tot va bé
int LOG_rebutMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n) {
	char buff[MAX_LINIA+100];
	char enter[33];
	char buffmissatge[MAX_LINIA];
	strncpy(buffmissatge, missatge, n);
	buff[0] = '\0';
	strcat(buff,IPloc);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPloc);
	strcat(buff, enter);
	strcat(buff," ha rebut un missatge de ");
	strcat(buff,IPrem);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPrem);
	strcat(buff, enter);
	strcat(buff, " -> ");
	strcat(buff, buffmissatge);
	strcat(buff, "(");
	sprintf(enter,"%d",n);
	strcat(buff, enter);
	strcat(buff, ")\n\0");
	return Log_Escriu(FitxLog,buff);
}

// missatge és un "string" de C (vector de chars imprimibles
// acabat en '\0')
// dest conte el primer caràcter de tipus concatenat amb missatge
// i sense el caràcter '\0'
// retorna la longitud de dest
// la longitud de dest ha de ser igual o major que la de missatge(comptant el caracter '\0')
int crearLinia(char * dest, char * tipus, const char * missatge) {
	dest[0] = tipus[0];
	int i = 0;
	while (missatge[i] != '\0') {
		dest[i+1] = missatge[i];
		i++;
	}
	i++;
	dest[i] = '\0';
	return i+1;
}

// copia src[1..n-1] a dest[0..n-2] i afegeix '\0' a dest[n-1]
void copiarInformacio(char * dest, char * src, int n) {
	int i;
	for(i = 0; i < n-1; i++) {
		dest[i] = src[i+1];
	}
	dest[n-1] = '\0';
}

// Pre: adrecaMI té el format nom@domini i acaba en '\0'. adrecaMI[0..n] n <= MAX_LINIA
// Post: nom s'emplena amb nom
//       domini s'emplena amb domini
//       tots dos acaben en '\0' i tenen mida màxima MAX_LINIA
void separarNomDomini(const char * adrecaMI, char * domini, char * nom) {
	int i = 0;
	while (adrecaMI[i] != '@') {
		nom[i] = adrecaMI[i];
		i++;
	}
	nom[i] = '\0';
	i++;
	int j = 0;
	while (adrecaMI[i] != '\0') {
		domini[j] = adrecaMI[i];
		i++;
		j++;
	}
	domini[j] = '\0';
}

/* AQUESTA FUNCIO ES FA SERVIR PER ALGUNA COSA??????????????????????????????????????????????????????????????*/
/* SEGURAMENT EL FAREM SERVIR AL NODE LUMI, AQUI NO CREC */
/* s'escriu a ip el contingut de buff[2] fins a la posició anterior a ':' + '\0' */
/* s'escriu a port el contingut posterior a ':' de buff fins a buff[n-1] */
/* retorna -1 si hi ha algun error, altrament retorna un valor positiu qualsevol */
int obtenirIPPortTCP(char * buff, int n, char * ip, int * port) {
	int i = 2;
	int j = 0;
	char p[6];
	while (buff[i] != ':' && i<n) {
		ip[j] = buff[i];
		i++;
		j++;
	}
	if (i>=n) {
		return -1;
	}
	j = 0;
	for (i=i+1; i<n; i++) {
		port[j] = buff [i];
		j++;
	}
	char* next;
	*port = strtol(p, &next, 10);
	return 1;
}

/* Si ho creieu convenient, feu altres funcions...                        */
