
#include "MIp2-funcions.h"
#include <stdio.h>
#include <string.h>
#include <ifaddrs.h>
#include <netdb.h>

// missatge és un "string" de C (vector de chars imprimibles
// acabat en '\0')
// dest conte el primer caràcter de tipus concatenat amb missatge
// i sense el caràcter '\0'
// retorna la longitud de dest
// la longitud de dest ha de ser igual o major que la de missatge(comptant el caracter '\0')
int crearLinia(char * dest, char * tipus, const char * missatge) {
	sprintf(dest, "%s%s", tipus, missatge);
	return strlen(dest);
}

// copia src[1..n-1] a dest[0..n-2] i afegeix '\0' a dest[n-1]
void copiarInformacio(char * dest, char * src, int n) {
	int i;
	for(i = 0; i < n-1; i++) {
		dest[i] = src[i+1];
	}
	dest[n-1] = '\0';
}

// Emplena IPloc amb l'adreça ip de la primera interficie que començi en 'w' o en 'e'
// Retorna 1 si tot va bé, altrament retorna -1;
int trobaAdrIpLocal(char * IPloc) {
	struct ifaddrs *ifaddr, *ifa;
	int s;
	char host[NI_MAXHOST];
	
	if (getifaddrs(&ifaddr) == -1) {
		perror("getifaddrs");
		return -1;
	}
	
	
	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr != NULL) {
			
			s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			
			if((ifa->ifa_name[0] == 'w' || ifa->ifa_name[0] == 'e')&&(ifa->ifa_addr->sa_family==AF_INET)) {
				if (s != 0) {
					return -1;
				}
				strcpy(IPloc, host);
				freeifaddrs(ifaddr);
				return 1;
			}
		}
	}
	freeifaddrs(ifaddr);
	return -1;
}




