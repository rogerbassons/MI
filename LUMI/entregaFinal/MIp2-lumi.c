/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer lumi.c que implementa la capa d'aplicació de LUMI, sobre la     */
/* de transport UDP (fent crides a la interfície de la capa UDP           */
/* -sockets-).                                                            */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <sys/types.h> o #include "meu.h" */
/*  (si les funcions externes es cridessin entre elles, faria falta fer   */
/*   un #include "MIp2-lumi.h")                                           */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>        
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <ifaddrs.h>

/* Definició de constants, p.e., #define MAX_LINIA 150                    */

#define PORT_NODE 60000
#define MAX_LINIA 300

/* Declaració de funcions internes que es fan servir en aquest fitxer     */
/* (les seves definicions es troben més avall) per així fer-les conegudes */
/* des d'aqui fins al final de fitxer.                                    */
/* Com a mínim heu de fer les següents funcions internes:                 */

int UDP_CreaSock(const char *IPloc, int portUDPloc);
int UDP_EnviaA(int Sck, const char *IPrem, int portUDPrem, const char *SeqBytes, int LongSeqBytes);
int UDP_RepDe(int Sck, char *IPrem, int *portUDPrem, char *SeqBytes, int LongSeqBytes);
int UDP_TancaSock(int Sck);
int UDP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portUDPloc);
int HaArribatAlgunaCosaEnTemps(const int *LlistaSck, int LongLlistaSck, int Temps);
int ResolDNSaIP(const char *NomDNS, char *IP);
int Log_CreaFitx(const char *NomFitxLog);
int Log_Escriu(int FitxLog, const char *MissLog);
int Log_TancaFitx(int FitxLog);

int enviarConfirmacio(int socketLoc, char * dades, char * IPrem, int * portUDPrem, int idFitxer);
int enviarLocalitzacio(int socketLoc, char * dades, char * IPrem, int * portUDPrem, int idFitxer);
int enviarUbicacio(int socketLoc, const char * codiError, const char * IPrem, int * portUDPrem, const char * ipUsuari, int * portUsuari, const char * usuRetorn, int idFitxer);
char rebrePeticio(int socketLoc, char * dades, char * IPrem, int * portUDPrem, int idFitxer);
int demanarLocalitzacioAgent(int sck, char * buff, char * IPrem, int * portUDPrem, char * ipUsuari, int * portUsuari, const char * usuRetorn, int idFitxer);
int demanarLocalitzacioNode();
int LOG_enviatMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n);
int LOG_rebutMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n);
int trobaAdrIpLocal(char *IPloc);
int crearLinia(char * dest, char * tipus, const char * missatge);
void copiarInformacio(char * dest, char * src, int n);
void separarNomDomini(const char * adrecaMI, char * domini, char * nom);
int buscarUsuari(const char * nom, char **nomsUsuaris, int nusuaris);
int obtenirIPPortTCP(char * buff, int n, char * ip, int * port);
int crearLiniaLocalitzar(char * dest, const char * missatge1, const char * missatge2);
int crearLiniaUbicacio(char * dest, const char * codiError, const char * ipUsuari, int portUsuari, const char * miRetorn);
void copiarDadesUbicacio(char * buff, char * ip, int * port);
int separarUsuariBuscatRetorn(const char * buff, char * usu1, char * usu2);

/* Definicio de funcions EXTERNES, és a dir, d'aquelles que en altres     */
/* fitxers externs es faran servir.                                       */
/* En termes de capes de l'aplicació, aquest conjunt de funcions externes */
/* formen la interfície de la capa LUMI.                                  */
/* Les funcions externes les heu de dissenyar vosaltres...                */

/* Descripció del que fa la funció...                                     */
/* Descripció dels arguments de la funció, què son, tipus, si es passen   */
/* per valor o per referència (la funció els omple)...                    */
/* Descripció dels valors de retorn de la funció...                       */

// Inicia l’escolta de peticions remotes de LUMI a través d’un nou
// socket UDP en el #port portUDPloc i a l'@IP IPloc. Si IPloc és "0.0.0.0" llavors
// la @IP del socket és qualsevol @IP local. Si portUDPloc és 0 llavors el #port
// s'assigna automàticament (pot ser qualsevol numero de port lliure)
// Obre un fitxer amb el nom fitxerLog i s'emplena idFitxer amb el seu identificador
// Retorna -1 si hi ha error, altrament retorna  l’identificador del socket d’escolta de LUMI creat si tot va bé
int LUMI_IniciaEscoltaPeticionsRemotes(const char *IPloc, int portUDPloc, const char * fitxerLog, int *idFitxer) {
	*idFitxer = Log_CreaFitx(fitxerLog);
	if (*idFitxer != 1) {
		return UDP_CreaSock(IPloc, portUDPloc);
	}
	else {
		return -1;
	}
}

// adrecaMI és l'adreça de l'usuari de MI que es vol registrar amb format: nom@domini
// Registra, mitjançant socket, l'usuari nom al node LUMI amb del domini domini i port PORT_NODE
// Retorna:
// 0 -> tot va bé
// 1 -> l'usuari no existeix
// Altrament retorna -1
int LUMI_Registrar(int socket, const char * adrecaMI, int idFitxer) {
	char ipnode[16];
	char iploc[16];
	int portudploc;
	char nomUsuariMI[MAX_LINIA];
	char domini[MAX_LINIA];
	separarNomDomini(adrecaMI, domini, nomUsuariMI);
	
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);

	if (ResolDNSaIP(domini, ipnode) == -1) {
		perror("Error en resoldre DNS a IP");
		return -1;
	}

	char missatgeEnviat[MAX_LINIA];
	int n = crearLinia(missatgeEnviat,"R",nomUsuariMI);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades

		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de registre
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			return -1;
		} else if (resultat != -2) {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer,iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}
	if (rebut && m == 2 && missatgeRebut[0] == 'C') {
		if (missatgeRebut[1] == '0') {
			return 0;
		} else if (missatgeRebut[1] == '1') {
			return 1;
		} else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI del missatge enviat"); // el missatge enviat no compleix el format PLUMI
			return -1;
		} else {
			perror("Format incorrecte PLUMI del missatge rebut");
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// adrecaMI és l'adreça de l'usuari de MI que es vol desregistrar amb format: nom@domini
// Desregistra, mitjançant socket, l'usuari nom al node LUMI amb del domini domini i port PORT_NODE
// Retorna:
// 0 -> tot va bé
// 1 -> l'usuari no existeix
// Altrament retorna -1
int LUMI_Desregistrar(int socket, const char * adrecaMI, int idFitxer)
{
	char ipnode[16];
	char iploc[16];
	int portudploc;
	char nomUsuariMI[MAX_LINIA];
	char domini[MAX_LINIA];
	separarNomDomini(adrecaMI, domini, nomUsuariMI);
	
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);
	
	if (ResolDNSaIP(domini, ipnode) == -1) {
		return -1;
	}
	
	char missatgeEnviat[MAX_LINIA];
	int n = crearLinia(missatgeEnviat,"D",nomUsuariMI);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades
		
		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de registre
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			return -1;
		} else if (resultat != -2) {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer, iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}
	if (rebut && m == 2 && missatgeRebut[0] == 'C') {
		if (missatgeRebut[1] == '0') {
			return 0;
		} else if (missatgeRebut[1] == '1') {
			return 1;
		} else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI del missatge enviat"); // el missatge enviat no compleix el format PLUMI
			return -1;
		} else {
			perror("Format incorrecte PLUMI del missatge rebut");
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// adrecaMIloc és l'adreça de l'usuari local amb format: nom1@domini1
// adrecaMIrem és l'adreça de l'usuari de MI que es vol localitzar amb format: nom2@domini2
// Localitza, mitjançant socket, l'usuari nom2 del domini domini2, a traves del node LUMI amb del domini domini1 i port PORT_NODE
// Emplena ip i port amb l'adreça ip i el port TCP de l'usuari de MI amb l'adreça adrecaMIrem.
// Retorna:
// 0 -> tot va bé
// -2 -> l'usuari no existeix
// -3 -> l'usuari està offline
// -4 -> l'usuari està en una conversa
// Altrament retorna -1
int LUMI_Localitzar(int socket, const char *adrecaMIloc, const char *adrecaMIrem , char * ip, int *port, int idFitxer) 
{
	char ipnode[16];
	char iploc[16];
	int portudploc;
	char nomUsuariMI[MAX_LINIA];
	char domini[MAX_LINIA];
	separarNomDomini(adrecaMIloc, domini, nomUsuariMI);
	
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);
	
	if (ResolDNSaIP(domini, ipnode) == -1) {
		return -1;
	}
	
	char missatgeEnviat[MAX_LINIA];
	int n = crearLiniaLocalitzar(missatgeEnviat, adrecaMIrem, adrecaMIloc);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades
		
		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de localització
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			return -1;
		} else if (resultat != -2) {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer, iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}

	if (rebut && m > 2 && missatgeRebut[0] == 'U') {
		if (missatgeRebut[1] == '0') {
			copiarDadesUbicacio(missatgeRebut, ip, port);
			return 0;
		}
		else if (missatgeRebut[1] == '1') {
			return -2;
		}
		else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI"); // el missatge enviat no compleix el format PLUMI
			return -1;
		}
		else if (missatgeRebut[1] == '3') { // l'usuari especificat al missatge està offline
			return -3;
		}
		else if (missatgeRebut[1] == '4') { // l'usuari especificat al missatge està offline
			return -4;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// S'espera indefinidament fins que arriba alguna cosa a socket o per teclat
// Si ha arribat alguna cosa a socket retorna socket, altrament retorna -1
int LUMI_HaArribatPeticio(int socket)
{
	int LlistaSck[2], LongLlistaSck;
	LlistaSck[0] = socket;
	LlistaSck[1] = 0;
	LongLlistaSck = 2;
	return HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  -1);
}


// Rep una petició dirigida a un domini nomDomini pel socket sck i serveix i envia una confirmació a l'usuari de la petició
// nomsUsuaris[0..nusuaris-1] té els noms de cada usuari (és un array de strings, s'ha de reservar l'espai per l'string dinàmicament)
// ipsUsuaris[0..nusuaris-1] té les ips de cada usuari   (és un array de strings, s'ha de reservar l'espai per l'string dinàmicament)
// portsUsuaris[0..nusuaris-1] té els ports de cada usuari
// si un usuari no està registrat llavors la ip i el port són 0
int LUMI_ServeixPeticio(int sck, char **nomsUsuaris, char **ipsUsuaris, int *portsUsuaris, int nusuaris, const char * nomDomini, int idFitxer) {
	char tipusPet;
	char buff[MAX_LINIA];
	char IPrem[16];
	int portUDPrem;
	char usuBuscat[MAX_LINIA];
	char domBuscat[MAX_LINIA];
	char nomBuscat[MAX_LINIA];
	char usuRetorn[MAX_LINIA];
	int zero = 0;
	tipusPet = rebrePeticio(sck, buff, IPrem, &portUDPrem, idFitxer);
	if (tipusPet == 'R' || tipusPet == 'D') {

		int i = buscarUsuari(buff, nomsUsuaris, nusuaris);
		if (i != -1) {
			if (tipusPet == 'R') {
				
				if (portsUsuaris[i] == 0) {
					ipsUsuaris[i] = malloc(strlen(IPrem) + 1);
					strcpy(ipsUsuaris[i], IPrem);
					portsUsuaris[i] = portUDPrem;
				}
				return enviarConfirmacio(sck, "0", IPrem, &portUDPrem, idFitxer);

			} else { //tipusPet == 'D'
				
				if (portsUsuaris[i] != 0) {
					ipsUsuaris[i] = malloc(strlen("0") + 1);
					strcpy(ipsUsuaris[i], "0");
					portsUsuaris[i] = 0;
				}
				return enviarConfirmacio(sck, "0", IPrem, &portUDPrem, idFitxer);
				
			}
		}
		else {
			return enviarConfirmacio(sck, "1", IPrem, &portUDPrem, idFitxer); //Usuari no existeix
		}
	}
	else if (tipusPet == 'L') {
		if (separarUsuariBuscatRetorn(buff, usuBuscat, usuRetorn) != 2) {
			return enviarUbicacio(sck, "2", IPrem, &portUDPrem, "0.0.0.0", &zero, "\0", idFitxer); //error format PLUMI
		}
		separarNomDomini(usuBuscat, domBuscat, nomBuscat);
		if (strcmp(domBuscat, nomDomini) == 0) { //buscar en aquest node
			int i = buscarUsuari(nomBuscat, nomsUsuaris, nusuaris);
			if (i != -1) {
				if (portsUsuaris[i] != 0) {
					return demanarLocalitzacioAgent(sck, buff, IPrem, &portUDPrem, ipsUsuaris[i], &portsUsuaris[i], usuRetorn, idFitxer);
				}
				else {
					return enviarUbicacio(sck, "3", IPrem, &portUDPrem, "0.0.0.0", &zero, usuRetorn, idFitxer); //usuari no registrat
				}
			}
			else {
				return enviarUbicacio(sck, "1", IPrem, &portUDPrem, "0.0.0.0", &zero, usuRetorn, idFitxer); //usuari no existeix
			}
		}
		else { 
			return demanarLocalitzacioNode(sck, domBuscat, buff, IPrem, &portUDPrem, usuRetorn, idFitxer);//demanar al node domBuscat
		}
	}
	else {
		return enviarConfirmacio(sck, "2", IPrem, &portUDPrem, idFitxer); // Format PLUMI incorrecte
	}
}
// Serveix una petició de resolucio de noms d'adreça a través del socket sck.
// La resolucio la serveix a través de sck amb la informació de la IPloc i portTCPloc
// Fa un log al fitxer amb identificador fitxer
// Retorna 1 si tot va bé, altrament retorna -1;
int LUMI_ServeixResolucio(int sck, char *IPloc, int portTCPloc, int fitxer) {
	char tipusPet;
	char buff[MAX_LINIA];
	char IPrem[16];
	int portUDPrem;
	int zero = 0;
	char usuBuscat[MAX_LINIA];
	char usuRetorn[MAX_LINIA];
	tipusPet = rebrePeticio(sck, buff, IPrem, &portUDPrem, fitxer);
	if (tipusPet == 'L') {
		if (separarUsuariBuscatRetorn(buff, usuBuscat, usuRetorn) != 2) {
			return enviarUbicacio(sck, "2", IPrem, &portUDPrem, "0.0.0.0", &zero, "\0", fitxer); //error format PLUMI
		}
		else {
			return enviarUbicacio(sck, "0", IPrem, &portUDPrem, IPloc, &portTCPloc, usuRetorn, fitxer);
		}
	}
	return 1;
}

// Envia una petició de localització a l'agent pel socket sck i posteriorment espera la resposta, rep i envia l'@IP i el port TCP a l'usuari de la petició
// buff conté les dades de la petició amb el format usuariBuscat:usuariRetorn
// IPrem conté l'@IP de l'agent consultor i portUDPrem el port UDP d'aquest
// ipUsuari conté l'@IP de l'agent consultat i portUsuari el port UDP d'aquest
// usuRetorn és el nom d'usuari de retorn amb el format "nom@domain" + '\0'
// retorna -1 si hi ha algun error; altrament retorna 0 si tot va bé
int demanarLocalitzacioAgent(int sck, char * buff, char * IPrem, int * portUDPrem, char * ipUsuari, int * portUsuari, const char * usuRetorn, int idFitxer) {
	char iploc[16];
	int portudploc;
	UDP_TrobaAdrSockLoc(sck, iploc, &portudploc);
	

	int rebut = 0;
	int m;
	char missatgeRebut[MAX_LINIA];
	enviarLocalitzacio(sck, buff, ipUsuari, portUsuari, idFitxer);
	int LlistaSck[1], LongLlistaSck;
	LlistaSck[0] = sck;
	LongLlistaSck = 1;
	int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
	if (resultat == -1) {
		return -1;
	} else if (resultat != -2) {
		m = UDP_RepDe(sck, ipUsuari, portUsuari, missatgeRebut, MAX_LINIA);
		if (m == -1) {
			return -1;
		}
		rebut = 1;
		LOG_rebutMissatge(idFitxer, iploc, PORT_NODE, ipUsuari, *portUsuari, missatgeRebut, m);
	}
	if (rebut && m > 2 && missatgeRebut[0] == 'U') {
		if (missatgeRebut[1] == '0') {
			char ip[16];
			int port;
			copiarDadesUbicacio(missatgeRebut, ip, &port);
			enviarUbicacio(sck, "0", IPrem, portUDPrem, ip, &port, usuRetorn, idFitxer); //OK
			return 0;
		}
		else if (missatgeRebut[1] == '1') {
			return 1;
		}
		else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI"); // el missatge enviat no compleix el format PLUMI
			return -1;
		}
		else if (missatgeRebut[1] == '3') {
			perror("Usuari offline (no registrat)"); // l'usuari especificat al missatge està offline
			return -1;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// Envia una petició de localització al node domini pel socket sck i posteriorment espera la resposta, rep i envia l'@IP i el port TCP a l'usuari de la petició
// buff conté les dades de la petició amb el format usuariBuscat:usuariRetorn
// IPrem conté l'@IP de l'agent consultor i portUDPrem el port UDP d'aquest
// usuRetorn és el nom d'usuari de retorn amb el format "nom@domain" + '\0'
int demanarLocalitzacioNode(int sck, char * domini, char * buff, char * IPrem, int * portUDPrem, char * usuRetorn, int idFitxer) {
	char iploc[16];
	int portudploc;
	char ipnode[16];
	
	UDP_TrobaAdrSockLoc(sck, iploc, &portudploc);
	
	if (ResolDNSaIP(domini, ipnode) == -1) {
		return -1;
	}

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
	int rebut = 0;
	
	enviarLocalitzacio(sck, buff, ipnode, &portudploc, idFitxer);
		
	int LlistaSck[1], LongLlistaSck;
	LlistaSck[0] = sck;
	LongLlistaSck = 1;
	int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
	if (resultat == -1) {
		return -1;
	} else if (resultat != -2) {
		m = UDP_RepDe(sck, ipnode, &portudploc, missatgeRebut, MAX_LINIA);
		if (m == -1) {
			return -1;
		}
		rebut = 1;
		LOG_rebutMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeRebut, m);
	}
	
	if (rebut && m > 2 && missatgeRebut[0] == 'U') {
		int port = 0;
		if (missatgeRebut[1] == '0') {
			char ip[16];
			copiarDadesUbicacio(missatgeRebut, ip, &port);
			enviarUbicacio(sck, "0", IPrem, portUDPrem, ip, &port, usuRetorn, idFitxer); //OK
			return 0;
		}
		else if (missatgeRebut[1] == '1') {
			enviarUbicacio(sck, "1", IPrem, portUDPrem, "0.0.0.0", &port, usuRetorn, idFitxer); 
			return 1;
		}
		else if (missatgeRebut[1] == '2') {
			enviarUbicacio(sck, "2", IPrem, portUDPrem, "0.0.0.0", &port, usuRetorn, idFitxer);  // el missatge enviat no compleix el format PLUMI
			return -1;
		}
		else if (missatgeRebut[1] == '3') {
			enviarUbicacio(sck, "3", IPrem, portUDPrem, "0.0.0.0", &port, usuRetorn, idFitxer); // l'usuari especificat al missatge està offline
			return -1;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// Tanca el socket UDP sck i el fitxer de log amb identificador idFitxer
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_FinalitzaEscoltaPeticionsRemotes(int Sck, int idFitxer) {
	if (UDP_TancaSock(Sck) == -1) {
		return -1;
	}
	if (Log_TancaFitx(idFitxer) == -1) {
		return -1;
	}
	return 1;
}

// Emplena dades amb la informació de la petició, dades acaba en '\0'
// Retorna el tipus de peticio si tot va bé, altrament retorna el caracter nul '\0'
char rebrePeticio(int socketLoc, char * dades, char * IPrem, int * portUDPrem, int idFitxer) {
	char iploc[16];
	int portudploc;
	char buff[MAX_LINIA];
	char tipusPet = '\0';
	
	int n = UDP_RepDe(socketLoc, IPrem, portUDPrem, buff, MAX_LINIA);
	if (n == -1) {
		return -1;
	} else {
		UDP_TrobaAdrSockLoc(socketLoc, iploc, &portudploc);
		LOG_rebutMissatge(idFitxer, iploc, portudploc, IPrem, *portUDPrem, buff, n);
		tipusPet = buff[0];
		copiarInformacio(dades, buff, n);
		return tipusPet;
	}
}

//Envia "C" + el contingut de dades a través del socket socketLoc al #portUDP portUDPrem i a la @IP IPrem 
//Retorna un positiu si tot va bé, -1 si hi ha algun error
int enviarConfirmacio(int socketLoc, char * dades, char * IPrem, int * portUDPrem, int idFitxer) {
	char dest[MAX_LINIA];
	char iploc[16];
	int portudploc;
	int n = crearLinia(dest, "C", dades);
	if (UDP_EnviaA(socketLoc, IPrem, *portUDPrem, dest, n) == -1) {
		return -1;
	} else {
		UDP_TrobaAdrSockLoc(socketLoc, iploc, &portudploc);
		LOG_enviatMissatge(idFitxer, iploc, portudploc, IPrem, *portUDPrem, dest, n);
		return n;
	}
}

//Envia "L" + el contingut de dades a través del socket socketLoc al #portUDP portUDPrem i a la @IP IPrem 
//Retorna un positiu si tot va bé, -1 si hi ha algun error
int enviarLocalitzacio(int socketLoc, char * dades, char * IPrem, int * portUDPrem, int idFitxer) {
	char dest[MAX_LINIA];
	char iploc[16];
	int portudploc;
	int n = crearLinia(dest, "L", dades);
	if (UDP_EnviaA(socketLoc, IPrem, *portUDPrem, dest, n) == -1) {
		return -1;
	} else {
		UDP_TrobaAdrSockLoc(socketLoc, iploc, &portudploc);
		LOG_enviatMissatge(idFitxer, iploc, portudploc, IPrem, *portUDPrem, dest, n);
		return n;
	}
}

//Envia "U" + el contingut de codiError + el contingut de ipUsuari + ':' + portUsuari a través del socket socketLoc al #portUDP portUDPrem i a la @IP IPrem 
//Retorna un positiu si tot va bé, -1 si hi ha algun error
int enviarUbicacio(int socketLoc, const char * codiError, const char * IPrem, int * portUDPrem, const char * ipUsuari, int * portUsuari, const char * usuRetorn, int idFitxer) {
	char dest[MAX_LINIA];
	char iploc[16];
	int portudploc;
	int n = crearLiniaUbicacio(dest, codiError, ipUsuari, *portUsuari, usuRetorn);
	if (UDP_EnviaA(socketLoc, IPrem, *portUDPrem, dest, n) == -1) {
		return -1;
	} else {
		UDP_TrobaAdrSockLoc(socketLoc, iploc, &portudploc);
		LOG_enviatMissatge(idFitxer, iploc, portudploc, IPrem, *portUDPrem, dest, n);
		return n;
	}
}

/* Definicio de funcions INTERNES, és a dir, d'aquelles que es faran      */
/* servir només en aquest mateix fitxer.                                  */

/* Crea un socket UDP a l’@IP “IPloc” i #port UDP “portUDPloc”            */
/* (si “IPloc” és “0.0.0.0” i/o “portUDPloc” és 0 es fa/farà una          */
/* assignació implícita de l’@IP i/o del #port UDP, respectivament).      */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int UDP_CreaSock(const char *IPloc, int portUDPloc)
{
	int socketUDP;
	if((socketUDP = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		return -1;
	}

	struct sockaddr_in adr;
	adr.sin_family= AF_INET;
	adr.sin_addr.s_addr= inet_addr(IPloc);
	adr.sin_port = htons(portUDPloc);
	int i;
	for(i=0;i<8;i++) {
		adr.sin_zero[i]='0';
	}

	if(bind(socketUDP, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(socketUDP);
		return -1;
	}
	
	return socketUDP;
}

/* Envia a través del socket UDP d’identificador “Sck” la seqüència de    */
/* bytes escrita a “SeqBytes” (de longitud “LongSeqBytes” bytes) cap al   */
/* socket remot que té @IP “IPrem” i #port UDP “portUDPrem”.              */
/* "IPrem" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* "SeqBytes" és un vector de chars qualsevol (recordeu que en C, un      */
/* char és un enter de 8 bits) d'una longitud >= LongSeqBytes bytes       */
/* Retorna -1 si hi ha error; el nombre de bytes enviats si tot va bé.    */
int UDP_EnviaA(int Sck, const char *IPrem, int portUDPrem, const char *SeqBytes, int LongSeqBytes)
{
	struct sockaddr_in adrrem;
	adrrem.sin_family= AF_INET; 
	adrrem.sin_addr.s_addr= inet_addr(IPrem); 
	adrrem.sin_port = htons(portUDPrem);
	int i;
	for(i=0;i<8;i++){adrrem.sin_zero[i]='0';}
	socklen_t long_adrrem = sizeof(adrrem);
	
	int bytes = sendto(Sck, SeqBytes, LongSeqBytes, 0, (struct sockaddr*) &adrrem, long_adrrem);
	if (bytes == -1) {
		perror("Error a sendto");
		return -1;
	}
	
	return bytes;
}

/* Rep a través del socket UDP d’identificador “Sck” una seqüència de     */
/* bytes que prové d'un socket remot i l’escriu a “SeqBytes*” (que té     */
/* una longitud de “LongSeqBytes” bytes).                                 */
/* Omple "IPrem*" i "portUDPrem*" amb respectivament, l'@IP i el #port    */
/* UDP del socket remot.                                                  */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* "SeqBytes*" és un vector de chars qualsevol (recordeu que en C, un     */
/* char és un enter de 8 bits) d'una longitud <= LongSeqBytes bytes       */
/* Retorna -1 si hi ha error; el nombre de bytes rebuts si tot va bé.     */
int UDP_RepDe(int Sck, char *IPrem, int *portUDPrem, char *SeqBytes, int LongSeqBytes)
{
	struct sockaddr_in adrrem;
	socklen_t long_adrrem = sizeof(adrrem);
		
	int bytes = recvfrom(Sck, SeqBytes, LongSeqBytes, 0, (struct sockaddr *) &adrrem, &long_adrrem);
	if (bytes == -1) {
		perror("Error a recvfrom");
		return -1;
	}
	strcpy(IPrem, inet_ntoa(adrrem.sin_addr));
	*portUDPrem = ntohs(adrrem.sin_port);
	return bytes;
}

/* S’allibera (s’esborra) el socket UDP d’identificador “Sck”.            */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int UDP_TancaSock(int Sck)
{
	return close(Sck);
}

/* Donat el socket UDP d’identificador “Sck”, troba l’adreça d’aquest     */
/* socket, omplint “IPloc*” i “portUDPloc*” amb respectivament, la seva   */
/* @IP i #port UDP.                                                       */
/* "IPloc*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int UDP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portUDPloc)
{
	struct sockaddr_in adrl;
	socklen_t long_adrl = sizeof(adrl);
	if (getsockname(Sck, (struct sockaddr *)&adrl, &long_adrl) == -1) {
		perror("Error en getsockname");
		return -1;
	}
	strcpy(IPloc, inet_ntoa(adrl.sin_addr));
	if (strcmp(IPloc,"0.0.0.0") == 0) {
		trobaAdrIpLocal(IPloc);
	}
	*portUDPloc = ntohs(adrl.sin_port);
	return 1;
}

/* Examina simultàniament durant "Temps" (en [ms] els sockets (poden ser  */
/* TCP, UDP i stdin) amb identificadors en la llista “LlistaSck” (de      */
/* longitud “LongLlistaSck” sockets) per saber si hi ha arribat alguna    */
/* cosa per ser llegida. Si Temps és -1, s'espera indefinidament fins que */
/* arribi alguna cosa.                                                    */
/* "LlistaSck" és un vector d'enters d'una longitud >= LongLlistaSck      */
/* Retorna -1 si hi ha error; retorna -2 si passa "Temps" sense que       */
/* arribi res; si arriba alguna cosa per algun dels sockets, retorna      */
/* l’identificador d’aquest socket.                                       */
int HaArribatAlgunaCosaEnTemps(const int *LlistaSck, int LongLlistaSck, int Temps)
{
	fd_set conjunt;
	FD_ZERO(&conjunt);

	struct timeval timeout;
	double segons = 0;
	if (Temps != -1) {
		segons = Temps / 1000;
	}
	
	int i;
	int descmax = 0;
	for (i = 0; i<LongLlistaSck; i++) {
		FD_SET(LlistaSck[i], &conjunt);
		if (descmax < LlistaSck[i])
			descmax = LlistaSck[i];
	}

	if (Temps != -1) {
		timeout.tv_sec = segons;
		timeout.tv_usec = 0;
		int res = select(descmax+1, &conjunt, NULL, NULL, &timeout);
		if (res == -1) {
			perror("Error en select");
			return -1;
		} else if (res == 0) {
			return -2;
		}
	} else {
		if(select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
			perror("Error en select");
			return -1;
		}
	}
	i = 0;
	while (i<LongLlistaSck && FD_ISSET(LlistaSck[i], &conjunt)==0)
		i++;

	if (i<LongLlistaSck) {
		return LlistaSck[i];
	} else {
		return -1;
	}
}

/* Donat el nom DNS "NomDNS" obté la corresponent @IP i l'escriu a "IP*"  */
/* "NomDNS" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud qualsevol, i "IP*" és un "string" de C (vector de */
/* chars imprimibles acabat en '\0') d'una longitud màxima de 16 chars    */
/* (incloent '\0').                                                       */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé     */
int ResolDNSaIP(const char *NomDNS, char *IP)
{
	struct hostent *dadesHOST;
	struct in_addr adrHOST;
	dadesHOST = gethostbyname(NomDNS);
	if (dadesHOST == NULL) {
		perror("No s'ha pogut resoldre el DNS");
		return -1;
	}
	adrHOST.s_addr = *((unsigned long *)dadesHOST->h_addr_list[0]);
	strcpy(IP,(char*)inet_ntoa(adrHOST));
	return 1;
}

/* Crea un fitxer de "log" de nom "NomFitxLog".                           */
/* "NomFitxLog" és un "string" de C (vector de chars imprimibles acabat   */
/* en '\0') d'una longitud qualsevol.                                     */
/* Retorna -1 si hi ha error; l'identificador del fitxer creat si tot va  */
/* bé.                                                                    */
int Log_CreaFitx(const char *NomFitxLog)
{
	int f = open(NomFitxLog, O_WRONLY | O_CREAT, 0666);
	if (f == -1) {
		perror("Error en obrir el fitxer");
		return -1;
	}
	return f;
}

/* Escriu al fitxer de "log" d'identificador "FitxLog" el missatge de     */
/* "log" "MissLog".                                                       */
/* "MissLog" és un "string" de C (vector de chars imprimibles acabat      */
/* en '\0') d'una longitud qualsevol.                                     */
/* Retorna -1 si hi ha error; el nombre de caràcters del missatge de      */
/* "log" (sense el '\0') si tot va bé                                     */
int Log_Escriu(int FitxLog, const char *MissLog)
{
	size_t llargada = strlen(MissLog);
	if (write(FitxLog, MissLog, llargada) == -1) {
		perror("Error en escriure al fitxer");
		return -1;
	}
	return llargada;
}

/* Tanca el fitxer de "log" d'identificador "FitxLog".                    */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int Log_TancaFitx(int FitxLog)
{
	return close(FitxLog);
}

// escriu al fitxer amb fd FitxLog la següent línia:
// "IPloc:portUDPloc ha enviat un missatge a IPrem:portUDPrem -> missatge(n)
// retorna 1 si tot va bé
int LOG_enviatMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n) {
	char buff[MAX_LINIA+100];
	sprintf(buff,"%s:%d ha enviat un missatge a %s:%d -> %s (%d bytes)\n", IPloc, portUDPloc, IPrem, portUDPrem, missatge, n);
	return Log_Escriu(FitxLog,buff);
}
// escriu al fitxer amb fd FitxLog la següent línia:
// "IPloc:portUDPloc ha rebut un missatge de IPrem:portUDPrem -> missatge(n)
// retorna 1 si tot va bé
int LOG_rebutMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n) {
	char buff[MAX_LINIA+100];
	char rebut[MAX_LINIA];
	strcpy(rebut, missatge);
	rebut[n] = '\0';
	sprintf(buff,"%s:%d ha rebut un missatge de %s:%d -> %s (%d bytes)\n", IPloc, portUDPloc, IPrem, portUDPrem, rebut, n);
	return Log_Escriu(FitxLog,buff);
}

// missatge és un "string" de C (vector de chars imprimibles acabat en '\0')
// dest conte 'L' concatenat amb missatge1, ':' i missatge2
// i sense el caràcter '\0'
// retorna la longitud de dest
// la longitud de dest ha de ser igual o major que la de missatge(comptant el caracter '\0')
int crearLiniaLocalitzar(char * dest, const char * missatge1, const char * missatge2) {
	sprintf(dest, "L%s:%s", missatge1, missatge2);
	return strlen(dest);
}

// missatge és un "string" de C (vector de chars imprimibles acabat en '\0')
// dest conte 'U' concatenat amb missatge1, ':' i missatge2
// i sense el caràcter '\0'
// retorna la longitud de dest
// la longitud de dest ha de ser igual o major que la de missatge(comptant el caracter '\0')
int crearLiniaUbicacio(char * dest, const char * codiError, const char * ipUsuari, int portUsuari, const char * miRetorn) {
	sprintf(dest, "U%s%s:%i:%s", codiError, ipUsuari, portUsuari, miRetorn);
	return strlen(dest);
}

// Pre: adrecaMI té el format nom@domini i acaba en '\0'. adrecaMI[0..n] n <= MAX_LINIA
// Post: nom s'emplena amb nom
//       domini s'emplena amb domini
//       tots dos acaben en '\0' i tenen mida màxima MAX_LINIA
void separarNomDomini(const char * adrecaMI, char * domini, char * nom) {
	sscanf(adrecaMI, "%[^@]@%s", nom, domini);
}

// Pre: buff conté dos vectors de chars dividits per ':'
// Post: usu1 s'emplena amb el vector abans de ':'
//       usu2 s'emplena amb el vector després de ':'
//       tots dos acaben en '\0' i tenen mida màxima MAX_LINIA
//Retorna 2 si tot va bé, altrament retorna un enter <2
int separarUsuariBuscatRetorn(const char * buff, char * usu1, char * usu2) {
	return sscanf(buff, "%[^:]:%s", usu1, usu2);
}

// nomsUsuaris[0..nusuaris] on nomsUsuaris[i] és el nom de l'usuari i
// si existeix nom == nomsUsuaris[i] llavors retorna i altrament retorna -1
int buscarUsuari(const char * nom, char **nomsUsuaris, int nusuaris) {
	int i = 0;
	while (strcmp(nom, nomsUsuaris[i]) != 0 && i < nusuaris) {
		i++;
	}
	if (i >= nusuaris) {
		return -1;
	} else {
		return i;
	}
}

// s'escriu a ip el contingut de buff[2] fins a la posició anterior a ':' + '\0'
// s'escriu a port el contingut posterior a ':' de buff fins a ':' + '\0'
void copiarDadesUbicacio(char * buff, char * ip, int * port) {
	char portChar[300];
	sscanf(buff, "U0%[^:]:%[^:]", ip, portChar);
	sscanf(portChar, "%d", port);
}

/* Si ho creieu convenient, feu altres funcions...                        */
