/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer nodelumi.c que implementa la interfície aplicació-administrador */
/* d'un node de LUMI, sobre la capa d'aplicació de LUMI (fent crides a la */
/* interfície de la capa LUMI -fitxers lumi.c i lumi.h-).                 */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp2-lumi.h" per poder fer crides a la interfície de LUMI     */
#include "MIp2-lumi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
/* Definició de constants, p.e., #define MAX_LINIA 150                    */
#define PORT_NODE 60000
#define N_USUARIS 10

int main(int argc,char *argv[])
{
	/* Declaració de variables, p.e., int n;                                 */
	char *nomsUsuaris[N_USUARIS];
	char *ipsUsuaris[N_USUARIS];
	int portsUsuaris[N_USUARIS];
	int i;
	char buff[300], nomDomini[300];
	int idFitxer;
	/* Expressions, estructures de control, crides a funcions, etc.          */

	FILE *fp = fopen("MIp2-nodelumi.cfg", "r");
	fscanf(fp, "%s", buff);
	strcpy(nomDomini, buff);
	for (i=0; i < N_USUARIS; i++) {
		fscanf(fp, "%s", buff);
		nomsUsuaris[i] = malloc(strlen(buff)+1);
		strcpy(nomsUsuaris[i], buff);
		ipsUsuaris[i] = malloc(strlen("0")+1);
		strcpy(ipsUsuaris[i], "0");
		portsUsuaris[i] = 0;
	}
	fclose(fp);


	int sck = LUMI_IniciaEscoltaPeticionsRemotes("0.0.0.0", 60000, "logNode", &idFitxer);

	while (1) {
		if (LUMI_HaArribatPeticio(sck) != sck) {
			perror("Error al esperar peticio");
			return -1;
		} else {
			LUMI_ServeixPeticio(sck, nomsUsuaris, ipsUsuaris, portsUsuaris, N_USUARIS, nomDomini, idFitxer);
		}
			
		//cal mirar si s'ha desregistrat algú amb timeout
	}
	LUMI_FinalitzaEscoltaPeticionsRemotes(sck, idFitxer);
}
