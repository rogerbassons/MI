//Roger Bassons Renart & Miquel Farreras Casamort

//SERVIDOR UDP

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main() {
	int s; //Socket
	char buff[200]; //Per a guardar el missatge rebut
	int numbytes; //Nombre de bytes rebuts
	int port; //Port UDP del servidor

	printf("Entra el port UDP del servidor\n");
    	scanf("%d", &port);
	
	if((s = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		exit(-1);
	}

	struct sockaddr_in adrloc;
	adrloc.sin_family= AF_INET;
	adrloc.sin_addr.s_addr= INADDR_ANY;
	adrloc.sin_port = htons(port);
	int i;
	for(i=0;i<8;i++){
		adrloc.sin_zero[i]='0';
	}

	if(bind(s, (struct sockaddr *)&adrloc, sizeof(adrloc)) == -1) {
		perror("Error en la crida bind\n");
		close(s);
		exit(-1);
	}

	struct sockaddr_in adrr;
	unsigned int long_adrr = sizeof(adrr);

	do {
		numbytes = recvfrom(s, buff, 200, 0, (struct sockaddr *) &adrr, &long_adrr);
		buff[numbytes] = '\0';
		printf("Missatge de %s:%d: %s\n", inet_ntoa(adrr.sin_addr), ntohs(adrr.sin_port), buff);
		sendto(s, buff, strlen(buff), 0, (struct sockaddr*) &adrr, long_adrr);
	} while (strcmp(buff, "FI") != 0);
	
	close(s);

	printf("Fi de la comunicació\n");

	return 0;
}
