/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer lumi.c que implementa la capa d'aplicació de LUMI, sobre la     */
/* de transport UDP (fent crides a la interfície de la capa UDP           */
/* -sockets-).                                                            */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <sys/types.h> o #include "meu.h" */
/*  (si les funcions externes es cridessin entre elles, faria falta fer   */
/*   un #include "MIp2-lumi.h")                                           */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>        
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>



/* Definició de constants, p.e., #define MAX_LINIA 150                    */

#define PORT_NODE 60000
#define MAX_LINIA 300

/* Declaració de funcions internes que es fan servir en aquest fitxer     */
/* (les seves definicions es troben més avall) per així fer-les conegudes */
/* des d'aqui fins al final de fitxer.                                    */
/* Com a mínim heu de fer les següents funcions internes:                 */

int UDP_CreaSock(const char *IPloc, int portUDPloc);
int UDP_EnviaA(int Sck, const char *IPrem, int portUDPrem, const char *SeqBytes, int LongSeqBytes);
int UDP_RepDe(int Sck, char *IPrem, int *portUDPrem, char *SeqBytes, int LongSeqBytes);
int UDP_TancaSock(int Sck);
int UDP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portUDPloc);
int HaArribatAlgunaCosaEnTemps(const int *LlistaSck, int LongLlistaSck, int Temps);
int ResolDNSaIP(const char *NomDNS, char *IP);
int Log_CreaFitx(const char *NomFitxLog);
int Log_Escriu(int FitxLog, const char *MissLog);
int Log_TancaFitx(int FitxLog);

int LOG_enviatMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n);
int LOG_rebutMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n);
int crearLinia(char * dest, char * tipus, const char * missatge);
void copiarInfo(char * dest, char * src, int inici, int fi);
void separarUserDomain(char * nomUsuLoc, char * dnsNode);
int obtenirIPPortTCP(char * buff, int n, char * ip, int * port);

/* Definicio de funcions EXTERNES, és a dir, d'aquelles que en altres     */
/* fitxers externs es faran servir.                                       */
/* En termes de capes de l'aplicació, aquest conjunt de funcions externes */
/* formen la interfície de la capa LUMI.                                  */
/* Les funcions externes les heu de dissenyar vosaltres...                */

/* Descripció del que fa la funció...                                     */
/* Descripció dels arguments de la funció, què son, tipus, si es passen   */
/* per valor o per referència (la funció els omple)...                    */
/* Descripció dels valors de retorn de la funció...                       */

// Inicia l’escolta de peticions remotes de LUMI a través d’un nou
// socket UDP en el #port portUDPloc i a l'@IP IPloc. Si IPloc és "0.0.0.0" llavors
// la @IP del socket és qualsevol @IP local. Si portUDPloc és 0 llavors el #port
// s'assigna automàticament (pot ser qualsevol numero de port lliure)
// Obre un fitxer amb el nom fitxerLog i s'emplena idFitxer amb el seu identificador
// Retorna -1 si hi ha error, altrament retorna  l’identificador del socket d’escolta de LUMI creat si tot va bé
int LUMI_IniciaEscoltaPeticionsRemotes(const char *IPloc, int portUDPloc, char * fitxerLog, int *idFitxer) {
	*idFitxer = Log_CreaFitx(fitxerLog);
	if (*idFitxer != 1) {
		return UDP_CreaSock(IPloc, portUDPloc);
	}
	else {
		return -1;
	}
}

// Registra nomUsuariMI i socket al node LUMI amb hostname dnsNode i port PORT_NODE
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_registrar(int socket, char * nomUsuariMI, char * dnsNode, int idFitxer) {
	char ipnode[16];
	char iploc[16];
	int portudploc;
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);
	
	if (ResolDNSaIP(dnsNode, ipnode) == -1) {
		perror("Error en resoldre DNS a IP");
		return -1;
	}
	
	char missatgeEnviat[MAX_LINIA];
	int n = crearLinia(missatgeEnviat,"R",nomUsuariMI);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades

		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de registre
			perror("Error en enviar petició al node DNS");
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			perror("Error en escoltar la resposta del node");
			return -1;
		} else if (resultat == -2) {
			perror("No s'ha rebut cap resposta del node");
			return -1;
		} else {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				perror("Error en rebre el missatge del node DNS");
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer,iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}
	if (rebut && m == 2 && missatgeRebut[0] == 'C') {
		if (missatgeRebut[1] == '0') {
			return 1;
		}
		else if (missatgeRebut[1] == '1') {
			perror("L'usuari no existeix");
			return -1;
		}
		else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI"); // el missatge enviat no compleix el format PLUMI
			return -1;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}

// Desregistra nomUsuariMI i socket al node LUMI amb hostname dnsNode i port PORT_NODE
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_desregistrar(int socket, char * nomUsuariMI, char * dnsNode, int idFitxer)
{
	char ipnode[16];
	char iploc[16];
	int portudploc;
	UDP_TrobaAdrSockLoc(socket, iploc, &portudploc);
	
	if (ResolDNSaIP(dnsNode, ipnode) == -1) {
		perror("Error en resoldre DNS a IP");
		return -1;
	}
	
	char missatgeEnviat[MAX_LINIA];
	int n = crearLinia(missatgeEnviat,"D",nomUsuariMI);

	char missatgeRebut[MAX_LINIA];
	int m; // mida de missatgeRebut
		
	int intents = 0;
	int rebut = 0;
	while (!rebut && intents < 3) { //ho intentem un màxim de 3 vegades
		
		LOG_enviatMissatge(idFitxer, iploc, portudploc, ipnode, PORT_NODE, missatgeEnviat, n);
		if (UDP_EnviaA(socket, ipnode, PORT_NODE, missatgeEnviat, n) == -1) { // enviem la petició de registre
			perror("Error en enviar petició al node DNS");
			return -1;
		}
		
		int LlistaSck[1], LongLlistaSck;
		LlistaSck[0] = socket;
		LongLlistaSck = 1;
		int resultat = HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  1000); // esperem un màxim de 1000ms per que arribi la resposta
		if (resultat == -1) {
			perror("Error en escoltar la resposta del node");
			return -1;
		} else if (resultat == -2) {
			perror("No s'ha rebut cap resposta del node");
			return -1;
		} else {
			char ip[16];
			int port;
			m = UDP_RepDe(socket, ip, &port, missatgeRebut, MAX_LINIA);
			if (m == -1) {
				perror("Error en rebre el missatge del node DNS");
				return -1;
			}
			rebut = 1;
			LOG_rebutMissatge(idFitxer, iploc, portudploc, ip, port, missatgeRebut, m);
		}
		intents++;
	}
	if (rebut && m == 2 && missatgeRebut[0] == 'C') {
		if (missatgeRebut[1] == '0') {
			return 1;
		}
		else if (missatgeRebut[1] == '1') {
			perror("L'usuari no existeix");
			return -1;
		}
		else if (missatgeRebut[1] == '2') {
			perror("Format incorrecte PLUMI"); // el missatge enviat no compleix el format PLUMI
			return -1;
		} else {
			return -1; // el missatge rebut no compleix el format PLUMI
		}
	} else {
		return -1; // el missatge no s'ha rebut o s'ha rebut però no compleix el format PLUMI
	}
}
/* Emplena ip i port amb l'adreça ip i el port de l'usuari de MI nomUsuariMI. */
/* Retorna 1 si tot va bé. */
int LUMI_localitzar(int socket, char *nomUsuariMI, char * ip, int *port) 
{
	
}


/* Escolta a socket fins que arriba una petició de registre o desregistre. */
/* Retorna socket si arriba una petició, altrament retorna -1. */
int LUMI_haArribatPeticio(int socket)
{
	int LlistaSck[1], LongLlistaSck;
	LlistaSck[0] = socket;
	LongLlistaSck = 1;
	return HaArribatAlgunaCosaEnTemps(LlistaSck, LongLlistaSck,  -1);
}
// Tanca el socket UDP sck i el fitxer de log amb identificador idFitxer
// Retorna 1 si tot va bé, altrament retorna -1
int LUMI_FinalitzaEscoltaPeticionsRemotes(int Sck, int idFitxer) {
	if (UDP_TancaSock(Sck) == -1) {
		perror("No s'ha pogut tancar el socket UDP");
		return -1;
	}
	if (Log_TancaFitx(idFitxer) == -1) {
		perror("No s'ha pogut tancar el fitxer de LOG");
		return -1;
	}
	return 1;
}

/* Definicio de funcions INTERNES, és a dir, d'aquelles que es faran      */
/* servir només en aquest mateix fitxer.                                  */

/* Crea un socket UDP a l’@IP “IPloc” i #port UDP “portUDPloc”            */
/* (si “IPloc” és “0.0.0.0” i/o “portUDPloc” és 0 es fa/farà una          */
/* assignació implícita de l’@IP i/o del #port UDP, respectivament).      */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int UDP_CreaSock(const char *IPloc, int portUDPloc)
{
	int socketUDP;
	if((socketUDP = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		return -1;
	}

	struct sockaddr_in adr;
	adr.sin_family= AF_INET;
	adr.sin_addr.s_addr= inet_addr(IPloc);
	adr.sin_port = htons(portUDPloc);
	int i;
	for(i=0;i<8;i++) {
		adr.sin_zero[i]='0';
	}

	if(bind(socketUDP, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(socketUDP);
		return -1;
	}
	
	return socketUDP;
}

/* Envia a través del socket UDP d’identificador “Sck” la seqüència de    */
/* bytes escrita a “SeqBytes” (de longitud “LongSeqBytes” bytes) cap al   */
/* socket remot que té @IP “IPrem” i #port UDP “portUDPrem”.              */
/* "IPrem" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* "SeqBytes" és un vector de chars qualsevol (recordeu que en C, un      */
/* char és un enter de 8 bits) d'una longitud >= LongSeqBytes bytes       */
/* Retorna -1 si hi ha error; el nombre de bytes enviats si tot va bé.    */
int UDP_EnviaA(int Sck, const char *IPrem, int portUDPrem, const char *SeqBytes, int LongSeqBytes)
{
	struct sockaddr_in adrrem;
	adrrem.sin_family= AF_INET; 
	adrrem.sin_addr.s_addr= inet_addr(IPrem); 
	adrrem.sin_port = htons(portUDPrem);
	int i;
	for(i=0;i<8;i++){adrrem.sin_zero[i]='0';}
	socklen_t long_adrrem = sizeof(adrrem);
	
	int bytes = sendto(Sck, SeqBytes, LongSeqBytes, 0, (struct sockaddr*) &adrrem, long_adrrem);
	
	
	return bytes;
}

/* Rep a través del socket UDP d’identificador “Sck” una seqüència de     */
/* bytes que prové d'un socket remot i l’escriu a “SeqBytes*” (que té     */
/* una longitud de “LongSeqBytes” bytes).                                 */
/* Omple "IPrem*" i "portUDPrem*" amb respectivament, l'@IP i el #port    */
/* UDP del socket remot.                                                  */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* "SeqBytes*" és un vector de chars qualsevol (recordeu que en C, un     */
/* char és un enter de 8 bits) d'una longitud <= LongSeqBytes bytes       */
/* Retorna -1 si hi ha error; el nombre de bytes rebuts si tot va bé.     */
int UDP_RepDe(int Sck, char *IPrem, int *portUDPrem, char *SeqBytes, int LongSeqBytes)
{
	struct sockaddr_in adrrem;
	socklen_t long_adrrem = sizeof(adrrem);
		
	int bytes = recvfrom(Sck, SeqBytes, LongSeqBytes, 0, (struct sockaddr *) &adrrem, &long_adrrem);

	strcpy(IPrem, inet_ntoa(adrrem.sin_addr));
	*portUDPrem = ntohs(adrrem.sin_port);
	return bytes;
}

/* S’allibera (s’esborra) el socket UDP d’identificador “Sck”.            */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int UDP_TancaSock(int Sck)
{
	return close(Sck);
}

/* Donat el socket UDP d’identificador “Sck”, troba l’adreça d’aquest     */
/* socket, omplint “IPloc*” i “portUDPloc*” amb respectivament, la seva   */
/* @IP i #port UDP.                                                       */
/* "IPloc*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int UDP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portUDPloc)
{
	struct sockaddr_in adrl;
	socklen_t long_adrl = sizeof(adrl);
	if (getsockname(Sck, (struct sockaddr *)&adrl, &long_adrl) == -1) {
		perror("Error en getsockname\n");
		return -1;
	}
	strcpy(IPloc, inet_ntoa(adrl.sin_addr));
	*portUDPloc = ntohs(adrl.sin_port);
	return 1;
}

/* Examina simultàniament durant "Temps" (en [ms] els sockets (poden ser  */
/* TCP, UDP i stdin) amb identificadors en la llista “LlistaSck” (de      */
/* longitud “LongLlistaSck” sockets) per saber si hi ha arribat alguna    */
/* cosa per ser llegida. Si Temps és -1, s'espera indefinidament fins que */
/* arribi alguna cosa.                                                    */
/* "LlistaSck" és un vector d'enters d'una longitud >= LongLlistaSck      */
/* Retorna -1 si hi ha error; retorna -2 si passa "Temps" sense que       */
/* arribi res; si arriba alguna cosa per algun dels sockets, retorna      */
/* l’identificador d’aquest socket.                                       */
int HaArribatAlgunaCosaEnTemps(const int *LlistaSck, int LongLlistaSck, int Temps)
{
	fd_set conjunt;
	FD_ZERO(&conjunt);

	struct timeval timeout;
	double segons = 0;
	if (Temps != -1) {
		segons = Temps / 1000;
	}
	
	int i;
	int descmax = 0;
	for (i = 0; i<LongLlistaSck; i++) {
		FD_SET(LlistaSck[i], &conjunt);
		if (descmax < LlistaSck[i])
			descmax = LlistaSck[i];
	}

	if (Temps != -1) {
		timeout.tv_sec = segons;
		timeout.tv_usec = 0;
		if(select(descmax+1, &conjunt, NULL, NULL, &timeout) == -1) {
			perror("Error en select\n");
			return -1;
		}
	} else {
		if(select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
			perror("Error en select\n");
			return -1;
		}
	}
	i = 0;
	while (i<LongLlistaSck && FD_ISSET(LlistaSck[i], &conjunt)==0)
		i++;

	if (i<LongLlistaSck) {
		return LlistaSck[i];
	} else {
		return -1;
	}
}

/* Donat el nom DNS "NomDNS" obté la corresponent @IP i l'escriu a "IP*"  */
/* "NomDNS" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud qualsevol, i "IP*" és un "string" de C (vector de */
/* chars imprimibles acabat en '\0') d'una longitud màxima de 16 chars    */
/* (incloent '\0').                                                       */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé     */
int ResolDNSaIP(const char *NomDNS, char *IP)
{
	struct hostent *dadesHOST;
	struct in_addr adrHOST;
	dadesHOST = gethostbyname(NomDNS);
	if (dadesHOST == NULL) {
		return -1;
	}
	adrHOST.s_addr = *((unsigned long *)dadesHOST->h_addr_list[0]);
	strcpy(IP,(char*)inet_ntoa(adrHOST));
	return 1;
}

/* Crea un fitxer de "log" de nom "NomFitxLog".                           */
/* "NomFitxLog" és un "string" de C (vector de chars imprimibles acabat   */
/* en '\0') d'una longitud qualsevol.                                     */
/* Retorna -1 si hi ha error; l'identificador del fitxer creat si tot va  */
/* bé.                                                                    */
int Log_CreaFitx(const char *NomFitxLog)
{
	int f = open(NomFitxLog, O_WRONLY | O_CREAT | O_TRUNC, 0666);
	if (f == -1) {
		perror("Error en obrir el fitxer\n");
		return -1;
	}
	return f;
}

/* Escriu al fitxer de "log" d'identificador "FitxLog" el missatge de     */
/* "log" "MissLog".                                                       */
/* "MissLog" és un "string" de C (vector de chars imprimibles acabat      */
/* en '\0') d'una longitud qualsevol.                                     */
/* Retorna -1 si hi ha error; el nombre de caràcters del missatge de      */
/* "log" (sense el '\0') si tot va bé                                     */
int Log_Escriu(int FitxLog, const char *MissLog)
{
	size_t llargada = strlen(MissLog);
	if (write(FitxLog, MissLog, llargada) == -1) {
		perror("Error en escriure al fitxer\n");
		return -1;
	}
	return llargada;
}

/* Tanca el fitxer de "log" d'identificador "FitxLog".                    */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int Log_TancaFitx(int FitxLog)
{
	return close(FitxLog);
}

// escriu al fitxer amb fd FitxLog la següent línia:
// "IPloc:portUDPloc ha enviat un missatge a IPrem:portUDPrem -> missatge(n)
// retorna 1 si tot va bé
int LOG_enviatMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n) {
	char buff[MAX_LINIA+100];
	char enter[33];
	char buffmissatge[MAX_LINIA];
	strncat(buffmissatge, missatge, n);
	buff[0] = '\0';
	strcat(buff,IPloc);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPloc);
	strcat(buff, enter);
	strcat(buff," ha enviat un missatge a ");
	strcat(buff,IPrem);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPrem);
	strcat(buff, enter);
	strcat(buff, " -> ");
	strcat(buff, buffmissatge);
	strcat(buff, "(");
	sprintf(enter,"%d",n);
	strcat(buff, enter);
	strcat(buff, ")");
	return Log_Escriu(FitxLog,buff);
}
// escriu al fitxer amb fd FitxLog la següent línia:
// "IPloc:portUDPloc ha rebut un missatge de IPrem:portUDPrem -> missatge(n)
// retorna 1 si tot va bé
int LOG_rebutMissatge(int FitxLog, const char *IPloc, int portUDPloc, const char *IPrem, int portUDPrem, const char *missatge, int n) {
	char buff[MAX_LINIA+100];
	char enter[33];
	char buffmissatge[MAX_LINIA];
	strncat(buffmissatge, missatge, n);
	buff[0] = '\0';
	strcat(buff,IPloc);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPloc);
	strcat(buff, enter);
	strcat(buff," ha rebut un missatge de ");
	strcat(buff,IPrem);
	strcat(buff,":");
	sprintf(enter,"%d",portUDPrem);
	strcat(buff, enter);
	strcat(buff, " -> ");
	strcat(buff, buffmissatge);
	strcat(buff, "(");
	sprintf(enter,"%d",n);
	strcat(buff, enter);
	strcat(buff, ")");
	return Log_Escriu(FitxLog,buff);
}

// missatge és un "string" de C (vector de chars imprimibles
// acabat en '\0')
// dest conte el primer caràcter de tipus concatenat amb missatge
// i sense el caràcter '\0'
// retorna la longitud de dest
// la longitud de dest ha de ser igual o major que la de missatge(comptant el caracter '\0')
int crearLinia(char * dest, char * tipus, const char * missatge) {
	int n = strlen(missatge)+1;
	dest[0] = tipus[0];
	int i;
	for(i = 0; i < n-1; i++) {
		dest[i+1] = missatge[i];
	}
	return n;
}

// copia src[inici..fi-1] a dest[0..fi-inici]
void copiarInfo(char * dest, char * src, int inici, int fi) {
	int i;
	for(i = 0; i < fi; i++) {
		dest[i] = src[inici+1];
	}
}

/* Amb "username"@"domain" a nomUsuLoc, deixa el username a nomUsuLoc i el nom DNS a dnsNode */
/* tots dos acaben en "\0" */
void separarUserDomain(char * nomUsuLoc, char * dnsNode) {
	int i = 0;
	while (nomUsuLoc[i] != '@') {
		i++;
	}
	nomUsuLoc[i] = '\0';
	int j = 0;
	while (nomUsuLoc[i] != '\0') {
		dnsNode[j] = nomUsuLoc[i];
		i++;
		j++;
	}
	dnsNode[j] = '\0';
}

/* s'escriu a ip el contingut de buff[2] fins a la posició anterior a ':' + '\0' */
/* s'escriu a port el contingut posterior a ':' de buff fins a buff[n-1] */
/* retorna -1 si hi ha algun error, altrament retorna un valor positiu qualsevol */
int obtenirIPPortTCP(char * buff, int n, char * ip, int * port) {
	int i = 2;
	int j = 0;
	char p[6];
	while (buff[i] != ':' && i<n) {
		ip[j] = buff[i];
		i++;
		j++;
	}
	if (i>=n) {
		return -1;
	}
	j = 0;
	for (i=i+1; i<n; i++) {
		port[j] = buff [i];
		j++;
	}
	char* next;
	*port = strtol(p, &next, 10);
	return 1;
}

/* Si ho creieu convenient, feu altres funcions...                        */
