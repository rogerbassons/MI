/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer capçalera de lumi.c                                             */
/*                                                                        */
/* Autors: Roger Bassons, Miquel Farreras                                 */
/*                                                                        */
/**************************************************************************/


/* Declaració de funcions externes de lumi.c, és a dir, d'aquelles que es */
/* faran servir en un altre fitxer extern, p.e., MIp2-p2p.c,              */
/* MIp2-nodelumi.c, o MIp2-agelumic.c. El fitxer extern farà un #include  */
/* del fitxer .h a l'inici, i així les funcions seran conegudes en ell.   */
/* En termes de capes de l'aplicació, aquest conjunt de funcions externes */
/* formen la interfície de la capa LUMI.                                  */
/* Les funcions externes les heu de dissenyar vosaltres...                */
int LUMI_IniciaEscoltaPeticionsRemotes(const char * IPloc, int portUDPloc, char * fitxerLog, int *idFitxer);
int LUMI_Registrar(int socket, char * nomUsuariMI, char * dnsNode, int idFitxer);
int LUMI_Desregistrar(int socket, char * nomUsuariMI, char * dnsNode, int idFitxer);
int LUMI_Localitzar(int socket, char * nomUsuariMI, char * ip, int * port, int idFitxer);
int LUMI_HaArribatPeticio(int socket);
int LUMI_FinalitzaEscoltaPeticionsRemotes(int Sck, int idFitxer);

