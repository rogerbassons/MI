/**************************************************************************/
/*                                                                        */
/* P1 - MI amb sockets TCP/IP - Part I                                    */
/* Fitxer p2p.c que implementa la interfície aplicació-usuari             */
/* (fa crides a la interfície de la capa MI (fitxers mi.c i mi.h)         */
/* Autors: Roger Bassons, Miquel Farreras                                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp1v4-mi.h" per poder fer crides a la interfície de MI       */
#include "MIp2-mi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
/* Definició de constants, p.e., #define MAX_LINIA 150                    */


int main(int argc,char *argv[])
{
	/* Declaració de variables, p.e., int n;                          */
	int port;
	char NicLoc[300], NicRem[300], adrecaMIloc[300], adrecaMIrem[300], nomFitxer[300];
	char IPloc[16], IPrem[16];
	int portTCPloc, portTCPrem;
	char buff[300];
	int n;
	int fitxer;
	int sck, sResolAdrecaMI, sConnectat, sPeticio;
	int acabar, conversaIniciada;

	/* Expressions, estructures de control, crides a funcions, etc.   */

	printf("Entra el nom del fitxer de log:\n");
	n = read(0,nomFitxer,300);
	nomFitxer[n-1] = '\0';

	printf("Entra la teva adreça de MI:\n");
	n = read(0,adrecaMIloc,300);
	adrecaMIloc[n-1] = '\0';
	
	printf("Entra el port TCP del servidor\n");
	scanf("%d", &port);
	
	int ret;
	if ((ret = MI_IniciaEscPetiRem(&sPeticio, &sResolAdrecaMI, adrecaMIloc, nomFitxer, &fitxer, port)) != 0) {
		if (ret == -1) {
			printf("Error en contactar amb el node\n");
		} else {
			printf("No s'ha pogut registrar: l'usuari no existeix\n");
		}
		return -1;
	}
	else {
		printf("S'ha registrat correctament\n");
	}

	printf("Entra el teu nickname:\n");
	n = read(0,NicLoc,300);
	NicLoc[n-1] = '\0';
	
	acabar = 0;
	conversaIniciada = 0;

	do {
		printf("Entra l'adreça MI de l'usuari amb el que vols parlar, FI per acabar, altrament espera\n");

		if ((sck = MI_HaArribatPeticio(sPeticio,sResolAdrecaMI)) == -1) {
			acabar = 1;
		}

		if (sck == 0) {
			n = read(0,adrecaMIrem,300);
			adrecaMIrem[n-1] = '\0';
			if (strcmp(adrecaMIrem,"FI") == 0) {
				acabar = 1;
			}
			else {
				if((sConnectat = MI_DemanaConv(sResolAdrecaMI, adrecaMIloc, adrecaMIrem, IPrem, &portTCPrem, IPloc, &portTCPloc, NicLoc, NicRem,fitxer)) < 0 ) {
					if (sConnectat == -1) {
						printf("Error en contactar per localitzar\n");
					} else if (sConnectat == -2) {
						printf("Error en demanar conversa: L'usuari especificat no existeix\n");
					} else if (sConnectat == -3) {
						printf("Error en demanar conversa: L'usuari especificat està offline\n");
					} else {
						printf("Error en demanar conversa: L'usuari està ocupat\n");
					}
					acabar = 1;
				}
				conversaIniciada = 1;
			}

		} else if (sck == sPeticio) {
			if((sConnectat = MI_AcceptaConv(sPeticio, IPrem, &portTCPrem, IPloc, &portTCPloc, NicLoc, NicRem, fitxer)) == -1) {
				acabar = 1;
			}
			conversaIniciada = 1;
		} else if (sck == sResolAdrecaMI) {
			if(MI_ServeixResolucioAdrecaMI(sPeticio, sResolAdrecaMI, fitxer) == -1) {
				acabar = 1;
			}
		}

		if (acabar == 0 && conversaIniciada == 1) {
			printf("El teu Nickname: %s\n", NicLoc);
			printf("Adreça IP local: %s\n", IPloc);
			printf("Port TCP local: %i\n", portTCPloc);
			printf("El Nickname remot: %s\n", NicRem);
			printf("Adreça IP remota: %s\n", IPrem);
			printf("Port TCP remot: %i\n", portTCPrem);

			printf("Escriu els missatges:\n");
			do {
				sck = MI_HaArribatLinia(sConnectat);
				if (sck == -1) {
					return -1;
				} else if (sck == 0) {
					n = read(0,buff, 300);
					buff[n-1] = '\0';
					if (strcmp(buff,"FI") != 0) {
						if (MI_EnviaLinia(sConnectat, buff) == -1) {
							printf("No s'ha pogut enviar la linia\n");
						}
					} else {
						n = -2;
					}
				} else if (sck == sConnectat) {
					if ((n = MI_RepLinia(sConnectat, buff)) != -2) {
						printf("%s diu: %s\n",NicRem,buff);
					}
				}

			} while (n != -2);
			MI_AcabaConv(sConnectat);
			conversaIniciada = 0;
			printf("Fi de la comunicació\n");
		}
	} while (acabar == 0);

	if ((ret = MI_AcabaEscPetiRem(sPeticio, sResolAdrecaMI, adrecaMIloc, fitxer)) != 0) {
		if (ret == -1) {
			printf("Error en contactar amb el node\n");
		} else { 
			printf("No s'ha pogut desregistrar: l'usuari no existeix\n");
		}
	}
	printf("Fi de l'aplicació\n");
	return 0;
}
