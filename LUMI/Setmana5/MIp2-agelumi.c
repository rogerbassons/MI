/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer agelumi.c que implementa la interfície aplicació-usuari d'un    */
/* agent d'usuari de LUMI sol, no integrat a l'aplicació de MI, sobre la  */
/* capa d'aplicació de LUMI (fent crides a la interfície de la capa LUMI  */
/* -fitxers lumi.c i lumi.h- ).                                           */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp2-lumi.h" per poder fer crides a la interfície de LUMI     */
#include "MIp2-lumi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Definició de constants, p.e., #define MAX_LINIA 150                    */


int main(int argc,char *argv[])
{
 /* Declaració de variables, p.e., int n;                                 */
	int idFitxer;
	char adrecaMI[300], adrecaMIrem[300], ip[16];
	int Sck;
	int n;
	int port;
	int acabar = 0;
	int portTCPloc = 1234; //PER PROVAR, NO TENIM PORT TCP AQUI!!!!
 /* Expressions, estructures de control, crides a funcions, etc.          */
	Sck = LUMI_IniciaEscoltaPeticionsRemotes("0.0.0.0", 0, "logAgent", &idFitxer);

	printf("Entra el teu usuari de MI:\n");
	n = read(0, adrecaMI,300);
	adrecaMI[n-1] = '\0';

	if (LUMI_Registrar(Sck, adrecaMI, idFitxer) != 0) {
		printf("No s'ha pogut registrar\n");
	}
	else {
		printf("Registrat\n");
	}
	do {
		printf("Entra l'usuari de MI que vols localitzar, espera per servir peticions, FI per acabar:\n");
		int socketRep = LUMI_HaArribatPeticio(Sck);
		if (socketRep == 0) { //localitzem usuari llegit per teclat
			n = read(0, adrecaMIrem,300);
			adrecaMIrem[n-1] = '\0';
			if (strcmp(adrecaMIrem, "FI") != 0) {
				if (LUMI_Localitzar(Sck, adrecaMI, adrecaMIrem, ip, &port, idFitxer) != 0) {
					printf("No s'ha pogut localitzar\n");
				}
				else {
					printf("IP: %s\n", ip);
					printf("Port: %i\n", port);
				}
			} else {
				acabar = 1;
			}
		}	
		else { //Servim petició localització
			LUMI_ServeixPeticio(Sck, NULL, NULL, &portTCPloc, 1, NULL, idFitxer);
			printf("S'ha servit una peticio\n");
		}
	} while (!acabar);
	if (LUMI_Desregistrar(Sck, adrecaMI, idFitxer) != 0) {
		printf("No s'ha pogut desregistrar\n");
	}
	else {
		printf("Desregistrat\n");
	}
	LUMI_FinalitzaEscoltaPeticionsRemotes(Sck, idFitxer);
	return 0;
}
