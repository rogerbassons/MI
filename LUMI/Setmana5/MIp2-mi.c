/**************************************************************************/
/*                                                                        */
/* P1 - MI amb sockets TCP/IP - Part I                                    */
/* Fitxer mi.c que implementa la capa d'aplicació de MI                   */
/* (fa crides a la interfície de la capa de transport TCP - sockets -)    */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <sys/types.h> o #include "meu.h" */
/*  (si les funcions externes es cridessin entre elles, faria falta fer   */
/*   un #include "MIp1v4-mi.h")                                           */
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ifaddrs.h>
#include "MIp2-mi.h"
#include "MIp2-lumi.h"
#include "MIp2-funcions.h"
/* Definició de constants, p.e., #define MAX_LINIA 150                    */

/* Declaració de funcions internes que es fan servir en aquest fitxer     */
/* (les seves definicions es troben més avall) per així fer-les conegudes */
/* des d'aqui fins al final de fitxer.                                    */
int TCP_CreaSockClient(const char *IPloc, int portTCPloc);
int TCP_CreaSockServidor(const char *IPloc, int portTCPloc);
int TCP_DemanaConnexio(int Sck, const char *IPrem, int portTCPrem);
int TCP_AcceptaConnexio(int Sck, char *IPrem, int *portTCPrem);
int TCP_Envia(int Sck, const char *SeqBytes, int LongSeqBytes);
int TCP_Rep(int Sck, char *SeqBytes, int LongSeqBytes);
int TCP_TancaSock(int Sck);
int TCP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portTCPloc);
int TCP_TrobaAdrSockRem(int Sck, char *IPrem, int *portTCPrem);
int HaArribatAlgunaCosa(const int *LlistaSck, int LongLlistaSck);


/* Definicio de funcions EXTERNES, és a dir, d'aquelles que en altres     */
/* fitxers externs es faran servir.                                       */
/* En termes de capes de l'aplicació, aquest conjunt de funcions externes */
/* formen la interfície de la capa MI.                                    */

// Inicia l’escolta de peticions remotes de conversa a través d’un nou    
// socket TCP en el #port “port" i una @IP local qualsevol (és a    
// dir, crea un socket “servidor” o en estat d’escolta – listen –).
// També Inicia l’escolta de peticions remotes de resolució de noms d'adreça MI
// a través d’un nou socket UDP en un #port qualsevol i una @IP local qualsevol (és a    
// dir, crea un socket “servidor” o en estat d’escolta – listen –).
// Emplena sPeticio amb l'identificador del socket d'escolta de peticions de conversa
// Emplena sResolAdrecaMI amb l'identificador del socket utilitzat per resoldre les ips i ports mitjançant l'adrecaMIloc
// Emplena fitxer amb l'identificador del fitxer de log
// Retorna -1 si hi ha error, 1 si l'usuari no existeix; altrament retorna 0  
int MI_IniciaEscPetiRem(int *sPeticio, int *sResolAdrecaMI, char *adrecaMIloc, char * nomFitxer, int *fitxer, int port)
{
	int ret;
	*sResolAdrecaMI = LUMI_IniciaEscoltaPeticionsRemotes("0.0.0.0", 0, nomFitxer, fitxer);
	if (*sResolAdrecaMI == -1) {
		return -1;
	}
	if ((ret = LUMI_Registrar(*sResolAdrecaMI, adrecaMIloc, *fitxer)) != 0) {
		return ret;
	}
	*sPeticio = TCP_CreaSockServidor("0.0.0.0",port);
	if (*sPeticio == -1) {
		return -1;
	}
	return 0;
}

// Escolta indefinidament fins que arriba una petició local de conversa   
// a través del teclat, una petició remota de conversa a través del   
// socket d’escolta de MI d’identificador “SckEscMI” (un socket “servidor”) o bé
// una petició remota de resolució d'adreça MI a través del socket sResolAdrecaMI.                                                       
// Retorna -1 si hi ha error; 0 si arriba una petició local; SckEscMI si
// arriba una petició remota; SResolAdrecaMI si arriba una peticio per resoldre l'adreça de mi                                           
int MI_HaArribatPeticio(int SckEscMI, int sResolAdrecaMI)
{
	int LlistaSck[3], LongLlistaSck;
	LlistaSck[0] = 0;
	LlistaSck[1] = SckEscMI;
	LlistaSck[2] = sResolAdrecaMI;
	LongLlistaSck = 3;
	return HaArribatAlgunaCosa(LlistaSck,LongLlistaSck);
}

// Crea una conversa iniciada per una petició local que arriba a través  
// del teclat: crea un socket TCP “client” (en un #port i @IP local      
// qualsevol), a través del qual fa una petició de conversa a un procés  
// remot, el qual les escolta a través del socket TCP ("servidor") d'@IP 
// “IPrem” i #port “portTCPrem” (és a dir, crea un socket “connectat” o  
// en estat establert – established –). Aquest socket serà el que es farà
// servir durant la conversa. Per trobar IPrem i portTCPrem s'utilitza el
// socket de resolució de noms d'adreçes MI sResolAdrecaMI, adrecaMIloc és
// l'adreça de MI local i adrecaMIrem és l'adreça de MI amb la qual es vol
// iniciar una conversació
// Omple “IPloc*” i “portTCPloc*” amb, respectivament, l’@IP i el #port  
// TCP del socket del procés local.
// Omple “IPrem*” i “portTCPrem*” amb, respectivament, l’@IP i el #port  
// TCP del socket del procés remot.  
// El nickname local “NicLoc” i el nickname remot són intercanviats amb  
// el procés remot, i s’omple “NickRem*” amb el nickname remot. El procés
// local és qui inicia aquest intercanvi (és a dir, primer s’envia el    
// nickname local i després es rep el nickname remot).                   
// "IPrem" i "IPloc*" són "strings" de C (vectors de chars imprimibles   
// acabats en '\0') d'una longitud màxima de 16 chars (incloent '\0')    
// "NicLoc" i "NicRem*" són "strings" de C (vectors de chars imprimibles 
// acabats en '\0') d'una longitud màxima de 300 chars (incloent '\0')
// Fa un log al fitxer amb identificador fitxer
// Retorna -1 si hi ha error; l’identificador del socket de conversa de  
// MI creat si tot va bé.                                                
int MI_DemanaConv(int sResolAdrecaMI, const char *adrecaMIloc, char *adrecaMIrem, char *IPrem, int *portTCPrem, char *IPloc, int *portTCPloc, const char *NicLoc, char *NicRem, int fitxer)
{
	int ret;
	if ((ret = LUMI_Localitzar(sResolAdrecaMI, adrecaMIloc, adrecaMIrem, IPrem, portTCPrem, fitxer)) != 0) {
		return ret;
	}
	int sConnectat = TCP_CreaSockClient("0.0.0.0",0);
	if (sConnectat == -1) {
		return -1;
	}
	if (TCP_DemanaConnexio(sConnectat,IPrem,*portTCPrem) == -1) {
		return -1;
	}
	if (TCP_TrobaAdrSockLoc(sConnectat,IPloc,portTCPloc) == -1) {
		return -1;
	}

	char buff[300];
	int n = crearLinia(buff,"N",NicLoc);
	if (TCP_Envia(sConnectat,buff,n) == -1) {
		return -1;
	}

	n = TCP_Rep(sConnectat,buff,300);
	if (n != -1 && buff[0] == 'N') {
		copiarInformacio(NicRem,buff,n);
	} else {
		return -1;
	}

	return sConnectat;
}

/* Crea una conversa iniciada per una petició remota que arriba a través  */
/* del socket d’escolta de MI d’identificador “SckEscMI” (un socket       */
/* “servidor”): accepta la petició i crea un socket (un socket            */
/* “connectat” o en estat establert – established –), que serà el que es  */
/* farà servir durant la conversa.                                        */
/* Omple “IPrem*”, “portTCPrem*”, “IPloc*” i “portTCPloc*” amb,           */
/* respectivament, l’@IP i el #port TCP del socket del procés remot i del */
/* socket del procés local.                                               */
/* El nickname local “NicLoc” i el nickname remot són intercanviats amb   */
/* el procés remot, i s’omple “NickRem*” amb el nickname remot. El procés */
/* remot és qui inicia aquest intercanvi (és a dir, primer es rep el      */
/* nickname remot i després s’envia el nickname local).                   */
/* "IPrem*" i "IPloc*" són "strings" de C (vectors de chars imprimibles   */
/* acabats en '\0') d'una longitud màxima de 16 chars (incloent '\0')     */
/* "NicLoc" i "NicRem*" són "strings" de C (vectors de chars imprimibles  */
/* acabats en '\0') d'una longitud màxima de 300 chars (incloent '\0')    */
/* Retorna -1 si hi ha error; l’identificador del socket de conversa      */
/* de MI creat si tot va bé.                                              */
int MI_AcceptaConv(int SckEscMI, char *IPrem, int *portTCPrem, char *IPloc, int *portTCPloc, const char* NicLoc, char *NicRem, int fitxer)
{
	int sConnectat = TCP_AcceptaConnexio(SckEscMI,IPrem,portTCPrem);
	if (sConnectat == -1) {
		return -1;
	}

	if (TCP_TrobaAdrSockLoc(sConnectat,IPloc,portTCPloc) == -1) {
		return -1;
	}

	if (TCP_TrobaAdrSockRem(sConnectat,IPrem,portTCPrem) == -1) {
		return -1;
	}
	char buff[300];
	int n = crearLinia(buff,"N",NicLoc);
	if (TCP_Envia(sConnectat,buff,n) == -1) {
		return -1;
	}

	n = TCP_Rep(sConnectat,buff,300);
	if (n != -1 && buff[0] == 'N') {
		copiarInformacio(NicRem,buff,n);
	} else {
		return -1;
	}

	return sConnectat;
}
// Serveix la petició remota de resolució de noms d'adreça MI que arriba a través
// del socket sResolAdrecaMI. La petició es serveix a través del socket sResolAdrecaMI
// amb la informació de la @IP i el #port del socket sPeticio
// Es fa un log al fitxer amb identificador fitxer
// Retorna 1 si tot va bé, altrament retorna -1
int MI_ServeixResolucioAdrecaMI(int sPeticio, int sResolAdrecaMI, int fitxer) {
	int portTCPloc;
	char IPloc[16];
	if (TCP_TrobaAdrSockLoc(sPeticio, IPloc, &portTCPloc) == -1) {
		return -1;
	}
	if (LUMI_ServeixResolucio(sResolAdrecaMI, IPloc, portTCPloc, fitxer) == -1) {
		return -1;
	}
	return 1;
}

/* Escolta indefinidament fins que arriba una línia local de conversa a   */
/* través del teclat o bé una línia remota de conversa a través del       */
/* socket de conversa de MI d’identificador “SckConvMI” (un socket        */
/* "connectat”).                                                          */
/* Retorna -1 si hi ha error; 0 si arriba una línia local; SckConvMI si   */
/* arriba una línia remota.                                               */
int MI_HaArribatLinia(int SckConvMI)
{
	int LlistaSck[2], LongLlistaSck;
	LlistaSck[0] = 0;
	LlistaSck[1] = SckConvMI;
	LongLlistaSck = 2;
	return HaArribatAlgunaCosa(LlistaSck,LongLlistaSck);
}

/* Envia a través del socket de conversa de MI d’identificador            */
/* “SckConvMI” (un socket “connectat”) la línia “Linia” escrita per       */
/* l’usuari local.                                                        */
/* "Linia" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0'), no conté el caràcter fi de línia ('\n') i té una longitud       */
/* màxima de 300 chars (incloent '\0')                                    */
/* Retorna -1 si hi ha error; el nombre de caràcters n de la línia        */
/* enviada (sense el ‘\0’) si tot va bé (0 <= n <= 299).                  */
int MI_EnviaLinia(int SckConvMI, const char *Linia)
{
	char buff[300];
	int n = crearLinia(buff,"L",Linia);
	if (TCP_Envia(SckConvMI,buff,n) == -1) {
		return -1;
	}
	return n-1;
}

/* Rep a través del socket de conversa de MI d’identificador “SckConvMI”  */
/* (un socket “connectat”) una línia escrita per l’usuari remot, amb la   */
/* qual omple “Linia”, o bé detecta l’acabament de la conversa per part   */
/* de l’usuari remot.                                                     */
/* "Linia*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0'), no conté el caràcter fi de línia ('\n') i té una longitud       */
/* màxima de 300 chars (incloent '\0')                                    */
/* Retorna -1 si hi ha error; -2 si l’usuari remot acaba la conversa; el  */
/* nombre de caràcters n de la línia rebuda (sense el ‘\0’) si tot va bé  */
/* (0 <= n <= 299).                                                       */
int MI_RepLinia(int SckConvMI, char *Linia)
{
	char buff[300];
	int n = TCP_Rep(SckConvMI,buff,300);
	if (n == -1) {
		return -1;
	} else if (n == 0) {
		return -2;
	} else if (buff[0] == 'L') {
		copiarInformacio(Linia,buff,n);
	}
	return n-1;
}

/* Acaba la conversa associada al socket de conversa de MI                */
/* d’identificador “SckConvMI” (un socket “connectat”).                   */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int MI_AcabaConv(int SckConvMI)
{
	return TCP_TancaSock(SckConvMI);
}

/* Acaba l’escolta de peticions remotes de conversa que arriben a través  */
/* del socket d’escolta de MI d’identificador “SckEscMI” (un socket       */
/* “servidor”) i acaba l'escola de peticions remotes de resolució de noms */
/* d'adreça MI a traves del socket sResolAdrecaMI amb adreça MI local adrecaMIloc*/
/* Tanca el fitxer de log amb identificador fitxer                        */
/* Retorna -1 si hi ha error, 1 si l'usuari adrecaMIloc no existeix; retorna 0 si tot va bé */
int MI_AcabaEscPetiRem(int SckEscMI, int sResolAdrecaMI, char *adrecaMIloc, int fitxer)
{	
	int ret;
	if ((ret = LUMI_Desregistrar(sResolAdrecaMI, adrecaMIloc, fitxer)) != 0) {
		return ret;
	}
	if (LUMI_FinalitzaEscoltaPeticionsRemotes(sResolAdrecaMI, fitxer) == -1) {
		return -1;
	}
	if (TCP_TancaSock(SckEscMI) == -1) {
		return -1;
	}
	return 0;
}

//Sck és l'identificador del socket local
//Omple portTCPloc amb el port del socket local
//Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.
int MI_TrobaPortEscoltaLocal(int Sck, int *portTCPloc) {
	char IPloc[16];
	return TCP_TrobaAdrSockLoc(Sck, IPloc, portTCPloc);
}

//Mostra totes les IPs de la màquina
void MI_MostrarIPs() {
	struct ifaddrs *ifap, *ifa;
	struct sockaddr_in *sa;
	char *addr;

	getifaddrs (&ifap);
	for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr->sa_family==AF_INET) {
			sa = (struct sockaddr_in *) ifa->ifa_addr;
			addr = inet_ntoa(sa->sin_addr);
			printf("Interfície: %s\tAdreça: %s\n", ifa->ifa_name, addr);
		}
	}
	freeifaddrs(ifap);
}

/* Definicio de funcions INTERNES, és a dir, d'aquelles que es faran      */
/* servir només en aquest mateix fitxer.                                  */

/* Crea un socket TCP “client” a l’@IP “IPloc” i #port TCP “portTCPloc”   */
/* (si “IPloc” és “0.0.0.0” i/o “portTCPloc” és 0 es fa/farà una          */
/* assignació implícita de l’@IP i/o del #port TCP, respectivament).      */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int TCP_CreaSockClient(const char *IPloc, int portTCPloc)
{
	struct sockaddr_in adr;
	adr.sin_family= AF_INET;
	adr.sin_addr.s_addr= inet_addr(IPloc);
	adr.sin_port = htons(portTCPloc);
	int i;
	for(i=0; i<8; i++) {
		adr.sin_zero[i]='0';
	}

	int scon;
	if((scon = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error en socket\n");
		return -1;
	}

	if (bind(scon, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(scon);
		return -1;
	}
	return scon;
}

/* Crea un socket TCP “servidor” (o en estat d’escolta – listen –) a      */
/* l’@IP “IPloc” i #port TCP “portTCPloc” (si “IPloc” és “0.0.0.0” i/o    */
/* “portTCPloc” és 0 es fa una assignació implícita de l’@IP i/o del      */
/* #port TCP, respectivament).                                            */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int TCP_CreaSockServidor(const char *IPloc, int portTCPloc)
{
	struct sockaddr_in adr;
	adr.sin_family= AF_INET;
	adr.sin_addr.s_addr= inet_addr(IPloc);
	adr.sin_port = htons(portTCPloc);
	int i;
	for(i=0; i<8; i++) {
		adr.sin_zero[i]='0';
	}

	int sPeticio;
	if ((sPeticio = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		return -1;
	}

	if (bind(sPeticio, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(sPeticio);
		return -1;
	}

	if (listen(sPeticio, 3) == -1) {
		perror("Error en listen\n");
		close(sPeticio);
		return -1;
	}
	return sPeticio;
}

/* El socket TCP “client” d’identificador “Sck” demana una connexió al    */
/* socket TCP “servidor” d’@IP “IPrem” i #port TCP “portTCPrem” (si tot   */
/* va bé es diu que el socket “Sck” passa a l’estat “connectat” o         */
/* establert – established –).                                            */
/* "IPrem" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_DemanaConnexio(int Sck, const char *IPrem, int portTCPrem)
{
	struct sockaddr_in adr;
	adr.sin_family= AF_INET;
	adr.sin_addr.s_addr= inet_addr(IPrem);
	adr.sin_port = htons(portTCPrem);
	int i;
	for(i=0; i<8; i++) {
		adr.sin_zero[i]='0';
	}

	if(connect(Sck, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en connect\n");
		close(Sck);
		return -1;
	}

	return Sck;
}

/* El socket TCP “servidor” d’identificador “Sck” accepta fer una         */
/* connexió amb un socket TCP “client” remot, i crea un “nou” socket,     */
/* que és el que es farà servir per enviar i rebre dades a través de la   */
/* connexió (es diu que aquest nou socket es troba en l’estat “connectat” */
/* o establert – established –; el nou socket té la mateixa adreça que    */
/* “Sck”).                                                                */
/* Omple “IPrem*” i “portTCPrem*” amb respectivament, l’@IP i el #port    */
/* TCP del socket remot amb qui s’ha establert la connexió.               */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket connectat creat  */
/* si tot va bé.                                                          */
int TCP_AcceptaConnexio(int Sck, char *IPrem, int *portTCPrem)
{
	struct sockaddr_in adrrem;
	socklen_t long_adrrem = sizeof(adrrem);
	int sConnectat;
	if ((sConnectat = accept(Sck,(struct sockaddr*)&adrrem, &long_adrrem)) == -1) {
		perror("Error en accept\n");
		close(Sck);
		return -1;
	}
	strcpy(IPrem, inet_ntoa(adrrem.sin_addr));
	*portTCPrem = ntohs(adrrem.sin_port);
	return sConnectat;
}

/* Envia a través del socket TCP “connectat” d’identificador “Sck” la     */
/* seqüència de bytes escrita a “SeqBytes” (de longitud “LongSeqBytes”    */
/* bytes) cap al socket TCP remot amb qui està connectat.                 */
/* "SeqBytes" és un vector de chars qualsevol (recordeu que en C, un      */
/* char és un enter de 8 bits) d'una longitud >= LongSeqBytes             */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; el nombre de bytes enviats si tot va bé.    */
int TCP_Envia(int Sck, const char *SeqBytes, int LongSeqBytes)
{
	return write(Sck, SeqBytes, LongSeqBytes);
}

/* Rep a través del socket TCP “connectat” d’identificador “Sck” una      */
/* seqüència de bytes que prové del socket remot amb qui està connectat,  */
/* i l’escriu a “SeqBytes*” (que té una longitud de “LongSeqBytes” bytes),*/
/* o bé detecta que la connexió amb el socket remot ha estat tancada.     */
/* "SeqBytes*" és un vector de chars qualsevol (recordeu que en C, un     */
/* char és un enter de 8 bits) d'una longitud <= LongSeqBytes             */
/* Retorna -1 si hi ha error; 0 si la connexió està tancada; el nombre de */
/* bytes rebuts si tot va bé.                                             */
int TCP_Rep(int Sck, char *SeqBytes, int LongSeqBytes)
{
	return read(Sck, SeqBytes, LongSeqBytes);
}

/* S’allibera (s’esborra) el socket TCP d’identificador “Sck”; si “Sck”   */
/* està connectat es tanca la connexió TCP que té establerta.             */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_TancaSock(int Sck)
{
	return close(Sck);
}

/* Donat el socket TCP d’identificador “Sck”, troba l’adreça d’aquest     */
/* socket, omplint “IPloc*” i “portTCPloc*” amb respectivament, la seva   */
/* @IP i #port TCP.                                                       */
/* "IPloc*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portTCPloc)
{
	struct sockaddr_in adrl;
	socklen_t long_adrl = sizeof(adrl);
	if (getsockname(Sck, (struct sockaddr *)&adrl, &long_adrl) == -1) {
		perror("Error en getsockname\n");
		return -1;
	}
	strcpy(IPloc, inet_ntoa(adrl.sin_addr));
	if (strcmp(IPloc,"0.0.0.0") == 0) {
		trobaAdrIpLocal(IPloc);
	}
	*portTCPloc = ntohs(adrl.sin_port);
	return 1;
}

/* Donat el socket TCP “connectat” d’identificador “Sck”, troba l’adreça  */
/* del socket remot amb qui està connectat, omplint “IPrem*” i            */
/* “portTCPrem*” amb respectivament, la seva @IP i #port TCP.             */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_TrobaAdrSockRem(int Sck, char *IPrem, int *portTCPrem)
{
	struct sockaddr_in adrr;
	socklen_t long_adrr = sizeof(adrr);
	if (getpeername(Sck, (struct sockaddr *)&adrr, &long_adrr) == -1) {
		perror("Error en getpeername\n");
		return -1;
	}
	strcpy(IPrem, inet_ntoa(adrr.sin_addr));
	*portTCPrem = ntohs(adrr.sin_port);
	return 1;
}

/* Examina simultàniament i sense límit de temps (una espera indefinida)  */
/* els sockets (TCP, UDP, stdin) de la llista d’identificadors de sockets */
/* “LlistaSck” (de longitud “LongLlistaSck” sockets) per saber si hi ha   */
/* arribat alguna cosa per ser llegida.                                   */
/* "LlistaSck" és un vector d'enters d'una longitud >= LongLlistaSck      */
/* Retorna -1 si hi ha error; si arriba alguna cosa per algun dels        */
/* sockets, retorna l’identificador d’aquest socket.                      */
int HaArribatAlgunaCosa(const int *LlistaSck, int LongLlistaSck)
{
	fd_set conjunt;
	FD_ZERO(&conjunt);
	int i;
	int descmax = 0;
	for (i = 0; i<LongLlistaSck; i++) {
		FD_SET(LlistaSck[i], &conjunt);
		if (descmax < LlistaSck[i])
			descmax = LlistaSck[i];
	}
	if(select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
		perror("Error en select\n");
		return -1;
	}
	i = 0;
	while (i<LongLlistaSck && FD_ISSET(LlistaSck[i], &conjunt)==0)
		i++;

	if (i<LongLlistaSck) {
		return LlistaSck[i];
	} else {
		return -1;
	}
}

/* Si ho creieu convenient, feu altres funcions...                        */
