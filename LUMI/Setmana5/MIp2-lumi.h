/**************************************************************************/
/*                                                                        */
/* P2 - MI amb sockets TCP/IP - Part II                                   */
/* Fitxer capçalera de lumi.c                                             */
/*                                                                        */
/* Autors: Roger Bassons, Miquel Farreras                                 */
/*                                                                        */
/**************************************************************************/


/* Declaració de funcions externes de lumi.c, és a dir, d'aquelles que es */
/* faran servir en un altre fitxer extern, p.e., MIp2-p2p.c,              */
/* MIp2-nodelumi.c, o MIp2-agelumic.c. El fitxer extern farà un #include  */
/* del fitxer .h a l'inici, i així les funcions seran conegudes en ell.   */
/* En termes de capes de l'aplicació, aquest conjunt de funcions externes */
/* formen la interfície de la capa LUMI.                                  */
/* Les funcions externes les heu de dissenyar vosaltres...                */

int LUMI_IniciaEscoltaPeticionsRemotes(const char * IPloc, int portUDPloc, const char * fitxerLog, int *idFitxer);
int LUMI_Registrar(int socket, const char * adrecaMI, int idFitxer);
int LUMI_Desregistrar(int socket, const char * adrecaMI, int idFitxer);
int LUMI_Localitzar(int socket, const char * adrecaMIloc, const char * adrecaMIrem, char * ip, int * port, int idFitxer);
int LUMI_HaArribatPeticio(int socket);
int LUMI_ServeixPeticio(int sck, char **nomsUsuaris, char **ipsUsuaris, int *portsUsuaris, int nusuaris, const char * nomDomini, int idFitxer);
int LUMI_ServeixResolucio(int sck, char *IPloc, int portTCPloc, int fitxer);
int LUMI_FinalitzaEscoltaPeticionsRemotes(int Sck, int idFitxer);

 
