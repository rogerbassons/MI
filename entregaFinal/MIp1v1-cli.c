#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <stdlib.h>
#include <string.h>

int main()
{
	printf("Entra la IP:\n");
	char ipserv[50];
	scanf("%s",ipserv);

	printf("Entra el Port:\n");
	int port;
	scanf("%d", &port);
	
	int scon;
	if((scon = socket(AF_INET, SOCK_STREAM, 0)) == -1) { 
		perror("Error en socket\n"); 
		exit(-1);   
	}

	struct sockaddr_in adrl;         /* adreça del socket local del client*/ 
	int long_adrl;                   /* longitud de l’adreça local adrl */ 
	struct sockaddr_in adrr;         /* adreça del socket remot del servidor per connect */ 
	int long_adrr;                   /* longitud de l’adreça remota adrr */ 

	long_adrl = sizeof(adrl); 
	if (getsockname(scon, (struct sockaddr *)&adrl, (socklen_t *)&long_adrl) == -1) {  
		perror("Error en getsockname\n"); 
		exit(-1); 
	} 
	printf("Sock LOC: @IP %s,TCP, #port %d\n",inet_ntoa(adrl.sin_addr),ntohs(adrl.sin_port));
	
	adrr.sin_family= AF_INET; 
	adrr.sin_addr.s_addr= inet_addr(ipserv); 
	adrr.sin_port = htons(port);
	int i;
	for(i=0;i<8;i++){adrr.sin_zero[i]='0';}
	
	if(connect(scon, (struct sockaddr *)&adrr, sizeof(adrr)) == -1) { 		
		perror("Error en connect\n");
		exit(-1); 
	} 

	
	char buff[200];
	char c;
	while((c = getchar()) != '\n' && c != EOF); // eliminar el \n que ha quedat al buffer despres de scanf
	do {
		printf("Missatge:\n");
		fgets(buff, 200, stdin);
		buff[strlen(buff)-1] = '\0';
		write(scon,buff,strlen(buff));
		
	} while (strcmp(buff, "FI") != 0);
	
	close(scon);
	return 0;
}	
