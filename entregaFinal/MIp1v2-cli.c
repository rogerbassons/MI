#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <stdlib.h>
#include <string.h>
#include "MIp1v2-Esquelet1.h"

int main()
{
	printf("Entra la IP:\n");
	char ipserv[50];
	int n = read(0,ipserv,50);
	ipserv[n-1] = '\0';

	printf("Entra el Port:\n");
	int port;
	scanf("%d",&port);
	printf("%s\n",ipserv);
	printf("%d\n",port);

	int scon = TCP_CreaSockClient("0.0.0.0",0);
	
	struct sockaddr_in adrr;         /* adreça del socket remot del servidor per connect */ 
	int long_adrr;                   /* longitud de l’adreça remota adrr */ 
	
	adrr.sin_family= AF_INET; 
	adrr.sin_addr.s_addr= inet_addr(ipserv); 
	adrr.sin_port = htons(port);
	int i;
	for(i=0;i<8;i++){adrr.sin_zero[i]='0';}
	
	if(connect(scon, (struct sockaddr *)&adrr, sizeof(adrr)) == -1) { 		
		perror("Error en connect\n");
		exit(-1); 
	} 

	char str[250];
	char buff[200];
	strcpy (str,"Nickname client: ");
	printf("Entra el teu nickname:\n");
	scanf("%s", buff);
	strcat (str, buff);
	str[strlen(str)] = '\0';
	write(scon,str,strlen(str));
	
	//char c;
	//while((c = getchar()) != '\n' && c != EOF); // eliminar el \n que ha quedat al buffer despres de scanf
	fd_set conjunt;
	FD_ZERO(&conjunt);
	int descmax = scon;
	printf("Escriu els missatges, no ens fem responsables legals en el cas que no arribin a la destinació:\n");
	do {
		FD_SET(0,&conjunt);
		FD_SET(scon,&conjunt);

		if (select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
			perror("Error en select\n"); 
			exit(-1); 
		}
		if (FD_ISSET(0,&conjunt)) {
			
			n = read(0,buff,200);
			buff[n-1] = '\0';
			write(scon,buff,n);
		}
		if (FD_ISSET(scon,&conjunt)) {
			n = read(scon,buff,200);
			printf("%s\n", buff);
		}
		
		
	} while (strcmp(buff, "FI") != 0);
	
	close(scon);
	return 0;
}	
