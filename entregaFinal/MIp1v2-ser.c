#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include "MIp1v2-Esquelet1.h"

int main() {
	int sConnectat; //Socket per la connexió amb el client
	fd_set conjunt; //Descriptors per al fitxer de lectura
	char buffer[200]; //Per a guardar el missatge rebut
	int numbytes; //Nombre de bytes rebuts
	int port; //Port TCP del servidor

	printf("Entra el port TCP del servidor\n");
    scanf("%d", &port);

	int sPeticio = TCP_CreaSockServidor("0.0.0.0",port);
	printf("Esperant peticions... (paciència)\n");
	
	struct sockaddr_in adrrem;
	int long_adrrem = sizeof(adrrem);
	if ((sConnectat = accept(sPeticio,(struct sockaddr*)&adrrem, &long_adrrem)) == -1) {
		perror("Error en accept\n");
		close(sPeticio);
		exit(-1);
	}
	printf("Connexio acceptada\n");

	/* quin codi més lleig aquí davant */
	char str[250];
	strcpy (str,"Nickname servidor: ");
	printf("Entra el teu nickname:\n");
	scanf("%s", buffer);
	strcat (str, buffer);
	str[strlen(str)] = '\0';
	write(sConnectat,str,strlen(str));
	/* codi lleig acabat */
	
	FD_ZERO(&conjunt);
	int descmax = sConnectat; // el número de descriptor màxim
	printf("Escriu els missatges, no ens fem responsables legals en el cas que no arribin a la destinació:\n");
	do {

		FD_SET(sConnectat,&conjunt);

		if (select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) { //examinem lectura del teclat i del socket sConnectat amb la llista conjunt
			perror("Error en select\n"); 
			exit(-1); 
		}
		if (FD_ISSET(0,&conjunt)) {
			int n = read(0,buffer,200);
			buffer[n-1] = '\0';
			write(sConnectat,buffer,n);
		}
		if (FD_ISSET(sConnectat,&conjunt)) {
			numbytes = read(sConnectat,buffer,200);
			printf("%s\n", buffer);
		}
	} while (strcmp(buffer, "FI") != 0);
	
	close(sConnectat);
	close(sPeticio);

	printf("Fi de la comunicació\n");

	return 0;
}

