#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

int main() {
	int sPeticio; //Socket per esperar peticions
	int sConnectat; //Socket per la connexió amb el client
	fd_set conjunt; //Descriptors per al fitxer de lectura
	char buffer[200]; //Per a guardar el missatge rebut
	int numbytes; //Nombre de bytes rebuts
	int port; //Port TCP del servidor

	printf("Entra el port TCP del servidor\n");
    	scanf("%d", &port);
	
	if((sPeticio = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		exit(-1);
	}

	struct sockaddr_in adrloc;
	adrloc.sin_family= AF_INET;
	adrloc.sin_addr.s_addr= INADDR_ANY;
	adrloc.sin_port = htons(port);
	int i;
	for(i=0;i<8;i++){
		adrloc.sin_zero[i]='0';
	}

	if(bind(sPeticio, (struct sockaddr *)&adrloc, sizeof(adrloc)) == -1) {
		perror("Error en la crida bind\n");
		close(sPeticio);
		exit(-1);
	}

	if(listen(sPeticio,3)==-1) {
		perror("Error en listen\n");
		close(sPeticio);
		exit(-1);
	}
	
	int long_adrloc = sizeof(adrloc);
	if (getsockname(sPeticio, (struct sockaddr *)&adrloc, &long_adrloc) == -1) {  
		perror("Error en getsockname\n"); 
		exit(-1); 
	}
	printf("Sock LOC: @IP %s,TCP, #port %d\n",inet_ntoa(adrloc.sin_addr),ntohs(adrloc.sin_port));

	struct sockaddr_in adrrem;
	int long_adrrem = sizeof(adrrem);
	if((sConnectat = accept(sPeticio,(struct sockaddr*)&adrrem, &long_adrrem))==-1) {
		perror("Error en accept\n");
		close(sPeticio);
		exit(-1);
	}
	printf("Connexio acceptada\n");
	
	FD_ZERO(&conjunt);

	do {
		FD_SET(sConnectat,&conjunt);
		if(FD_ISSET (sConnectat,&conjunt)) {
			numbytes = read(sConnectat,buffer,200);
			buffer[numbytes] = '\0';
			printf("%s\n",buffer);
		}
	} while (strcmp(buffer, "FI") != 0);
	
	close(sConnectat);
	close(sPeticio);

	printf("Fi de la comunicació\n");

	return 0;
}

