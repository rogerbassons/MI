/**************************************************************************/
/*                                                                        */
/* P1 - MI amb sockets TCP/IP - Part I                                    */
/* Versio numero N                                                        */
/* */
/* Autors: Roger Bassons Renart, Miquel Farreras Casamort                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
/* Definició de constants, p.e., #define MAX_LINIA 150                    */

/* Declaració de funcions (les seves definicions es troben més avall) per */
/* així fer-les conegudes des d'aqui fins al final de fitxer.             */
int TCP_CreaSockClient(const char *IPloc, int portTCPloc);
int TCP_CreaSockServidor(const char *IPloc, int portTCPloc);
int TCP_DemanaConnexio(int Sck, const char *IPrem, int portTCPrem);
int TCP_AcceptaConnexio(int Sck, char *IPrem, int *portTCPrem);
int TCP_Envia(int Sck, const char *SeqBytes, int LongSeqBytes);
int TCP_Rep(int Sck, char *SeqBytes, int LongSeqBytes);
int TCP_TancaSock(int Sck);
int TCP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portTCPloc);
int TCP_TrobaAdrSockRem(int Sck, char *IPrem, int *portTCPrem);
int HaArribatAlgunaCosa(const int *LlistaSck, int LongLlistaSck);

int main(int argc,char *argv[])
{
	/* Declaració de variables, p.e., int n */
	int port;
	int sPeticio, sConnectat, sck;
	int LlistaSck[2], LongLlistaSck;
	char ipserv[16];
	int n;
	char buff[300];
	
	/* Expressions, estructures de control, crides a funcions, etc. */
	printf("Entra el port TCP del servidor\n");
	scanf("%d", &port);
	sPeticio = TCP_CreaSockServidor("0.0.0.0",port);

	printf("Entra la ip per a connectar-te, altrament espera\n");

	LlistaSck[0] = 0;
	LlistaSck[1] = sPeticio;
	LongLlistaSck = 2;
	
	sck = HaArribatAlgunaCosa(LlistaSck,LongLlistaSck);
	if (sck == 0) {
		n = read(0,ipserv,16);
		ipserv[n-1] = '\0';

		printf("Entra el Port:\n");
		scanf("%d", &port);
	
		sConnectat = TCP_CreaSockClient("0.0.0.0",0);
		TCP_DemanaConnexio(sConnectat,ipserv,port);
		
	} else if (sck == sPeticio) {
		sConnectat = TCP_AcceptaConnexio(sPeticio,ipserv,&port);
	}
	TCP_TancaSock(sPeticio);

	LlistaSck[0] = 0;
	LlistaSck[1] = sConnectat;
	LongLlistaSck = 2;
	
	printf("Escriu els missatges, no ens fem responsables legals en el cas que no arribin a la destinació:\n");

	do {
		sck = HaArribatAlgunaCosa(LlistaSck,LongLlistaSck);
		if (sck == 0) {
			n = read(0,buff, 300);
			buff[n-1] = '\0';
			TCP_Envia(0,buff,n);
		} else if (sck == sConnectat) {
			TCP_Rep(sConnectat,buff,300);
			printf("%s\n", buff);
		}
	} while (1);
	TCP_TancaSock(sConnectat);
	
	printf("Fi de la comunicació\n");
	return 0;
}


/* Definicio de funcions                                                  */

/* Crea un socket TCP “client” a l’@IP “IPloc” i #port TCP “portTCPloc”   */
/* (si “IPloc” és “0.0.0.0” i/o “portTCPloc” és 0 es fa/farà una          */
/* assignació implícita de l’@IP i/o del #port TCP, respectivament).      */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int TCP_CreaSockClient(const char *IPloc, int portTCPloc)
{
	struct sockaddr_in adr;
	adr.sin_family= AF_INET; 
	adr.sin_addr.s_addr= inet_addr(IPloc); 
	adr.sin_port = htons(portTCPloc);
	int i;
	for(i=0;i<8;i++){adr.sin_zero[i]='0';}
	
	int scon;
	if((scon = socket(AF_INET, SOCK_STREAM, 0)) == -1) { 
		perror("Error en socket\n"); 
		return -1;  
	}

	if (bind(scon, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(scon);
		return -1;
	}
	return scon;
}

/* Crea un socket TCP “servidor” (o en estat d’escolta – listen –) a      */
/* l’@IP “IPloc” i #port TCP “portTCPloc” (si “IPloc” és “0.0.0.0” i/o    */
/* “portTCPloc” és 0 es fa una assignació implícita de l’@IP i/o del      */
/* #port TCP, respectivament).                                            */
/* "IPloc" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket creat si tot     */
/* va bé.                                                                 */
int TCP_CreaSockServidor(const char *IPloc, int portTCPloc)
{
	struct sockaddr_in adr;
	adr.sin_family= AF_INET; 
	adr.sin_addr.s_addr= inet_addr(IPloc); 
	adr.sin_port = htons(portTCPloc);
	int i;
	for(i=0;i<8;i++){adr.sin_zero[i]='0';}

	int sPeticio;
	if ((sPeticio = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error en la crida socket\n");
		return -1;
	}

      	if (bind(sPeticio, (struct sockaddr *)&adr, sizeof(adr)) == -1) {
		perror("Error en la crida bind\n");
		close(sPeticio);
		return -1;
	}

	if (listen(sPeticio, 3) == -1) {
		perror("Error en listen\n");
		close(sPeticio);
		return -1;
	}
	return sPeticio;
}

/* El socket TCP “client” d’identificador “Sck” demana una connexió al    */
/* socket TCP “servidor” d’@IP “IPrem” i #port TCP “portTCPrem” (si tot   */
/* va bé es diu que el socket “Sck” passa a l’estat “connectat” o         */
/* establert – established –).                                            */
/* "IPrem" és un "string" de C (vector de chars imprimibles acabat en     */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_DemanaConnexio(int Sck, const char *IPrem, int portTCPrem)
{
	struct sockaddr_in adr;
	adr.sin_family= AF_INET; 
	adr.sin_addr.s_addr= inet_addr(IPrem); 
	adr.sin_port = htons(portTCPrem);
	int i;
	for(i=0;i<8;i++){adr.sin_zero[i]='0';}

	if(connect(Sck, (struct sockaddr *)&adr, sizeof(adr)) == -1) { 		
		perror("Error en connect\n");
		close(Sck);
		return -1;
	}
	
	return Sck;
}

/* El socket TCP “servidor” d’identificador “Sck” accepta fer una         */
/* connexió amb un socket TCP “client” remot, i crea un “nou” socket,     */
/* que és el que es farà servir per enviar i rebre dades a través de la   */
/* connexió (es diu que aquest nou socket es troba en l’estat “connectat” */
/* o establert – established –; el nou socket té la mateixa adreça que    */
/* “Sck”).                                                                */
/* Omple “IPrem*” i “portTCPrem*” amb respectivament, l’@IP i el #port    */
/* TCP del socket remot amb qui s’ha establert la connexió.               */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; l’identificador del socket connectat creat  */
/* si tot va bé.                                                          */
int TCP_AcceptaConnexio(int Sck, char *IPrem, int *portTCPrem)
{
	struct sockaddr_in adrrem;
	int long_adrrem = sizeof(adrrem);
	int sConnectat;
	if ((sConnectat = accept(Sck,(struct sockaddr*)&adrrem, &long_adrrem)) == -1) {
		perror("Error en accept\n");
		close(Sck);
		return -1;
	}
	return sConnectat;
}

/* Envia a través del socket TCP “connectat” d’identificador “Sck” la     */
/* seqüència de bytes escrita a “SeqBytes” (de longitud “LongSeqBytes”    */
/* bytes) cap al socket TCP remot amb qui està connectat.                 */
/* "SeqBytes" és un vector de chars qualsevol (recordeu que en C, un      */
/* char és un enter de 8 bits) d'una longitud >= LongSeqBytes             */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; el nombre de bytes enviats si tot va bé.    */
int TCP_Envia(int Sck, const char *SeqBytes, int LongSeqBytes)
{
	return write(Sck, SeqBytes, LongSeqBytes);
}

/* Rep a través del socket TCP “connectat” d’identificador “Sck” una      */
/* seqüència de bytes que prové del socket remot amb qui està connectat,  */
/* i l’escriu a “SeqBytes*” (que té una longitud de “LongSeqBytes” bytes),*/
/* o bé detecta que la connexió amb el socket remot ha estat tancada.     */
/* "SeqBytes*" és un vector de chars qualsevol (recordeu que en C, un     */
/* char és un enter de 8 bits) d'una longitud <= LongSeqBytes             */
/* Retorna -1 si hi ha error; 0 si la connexió està tancada; el nombre de */
/* bytes rebuts si tot va bé.                                             */
int TCP_Rep(int Sck, char *SeqBytes, int LongSeqBytes)
{
	return read(Sck, SeqBytes, LongSeqBytes);
}

/* S’allibera (s’esborra) el socket TCP d’identificador “Sck”; si “Sck”   */
/* està connectat es tanca la connexió TCP que té establerta.             */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_TancaSock(int Sck)
{
	return close(Sck);
}

/* Donat el socket TCP d’identificador “Sck”, troba l’adreça d’aquest     */
/* socket, omplint “IPloc*” i “portTCPloc*” amb respectivament, la seva   */
/* @IP i #port TCP.                                                       */
/* "IPloc*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_TrobaAdrSockLoc(int Sck, char *IPloc, int *portTCPloc)
{
	struct sockaddr_in adrl;
	int long_adrl = sizeof(adrl);
	if (getsockname(Sck, (struct sockaddr *)&adrl, &long_adrl) == -1) {
		perror("Error en getsockname\n");
		return -1;
	}
	IPloc = inet_ntoa(adrl.sin_addr);
	*portTCPloc = ntohs(adrl.sin_port);
	return *portTCPloc;
}

/* Donat el socket TCP “connectat” d’identificador “Sck”, troba l’adreça  */
/* del socket remot amb qui està connectat, omplint “IPrem*” i            */
/* “portTCPrem*” amb respectivament, la seva @IP i #port TCP.             */
/* "IPrem*" és un "string" de C (vector de chars imprimibles acabat en    */
/* '\0') d'una longitud màxima de 16 chars (incloent '\0')                */
/* Retorna -1 si hi ha error; un valor positiu qualsevol si tot va bé.    */
int TCP_TrobaAdrSockRem(int Sck, char *IPrem, int *portTCPrem)
{
	struct sockaddr_in adrr;
	int long_adrr = sizeof(adrr);
	if (getpeername(Sck, (struct sockaddr *)&adrr, &long_adrr) == -1) {
		perror("Error en getpeername\n");
		return -1;
	}
	IPrem = inet_ntoa(adrr.sin_addr);
	*portTCPrem = ntohs(adrr.sin_port);
	return *portTCPrem;
}

/* Examina simultàniament i sense límit de temps (una espera indefinida)  */
/* els sockets (TCP, UDP, stdin) de la llista d’identificadors de sockets */
/* “LlistaSck” (de longitud “LongLlistaSck” sockets) per saber si hi ha   */
/* arribat alguna cosa per ser llegida.                                   */
/* "LlistaSck" és un vector d'enters d'una longitud >= LongLlistaSck      */
/* Retorna -1 si hi ha error; si arriba alguna cosa per algun dels        */
/* sockets, retorna l’identificador d’aquest socket.                      */
int HaArribatAlgunaCosa(const int *LlistaSck, int LongLlistaSck)
{
	fd_set conjunt;
	FD_ZERO(&conjunt);
	int i;
	int descmax = 0;
	for (i = 0; i<LongLlistaSck; i++) {
		FD_SET(LlistaSck[i], &conjunt);
		if (descmax < LlistaSck[i])
			descmax = LlistaSck[i];
	}
	if(select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
		perror("Error en select\n");
		return -1;
	}
	i = 0;
	while (i<LongLlistaSck && FD_ISSET(LlistaSck[i], &conjunt)==0)
		i++;

	if (i<LongLlistaSck)
		return LlistaSck[i];
	else 
		return -1;
}

/* Si ho creieu convenient, feu altres funcions...                        */
