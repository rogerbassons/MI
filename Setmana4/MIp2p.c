#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	int port = 0;
	printf("Entra el port TCP del servidor\n");
	scanf("%d", &port);
	int sPeticio = TCP_CreaSockServidor("0.0.0.0",port);
	int sConnectat = 0;
	struct sockaddr_in adrr;         /* adreça del socket remot del servidor per connect */
	int long_adrr;                   /* longitud de l’adreça remota adrr */
	fd_set conjunt;
	printf("Prem enter per a connectar amb un procés escoltant, altrament espera\n");
	FD_ZERO(&conjunt);
	FD_SET(0,&conjunt);
	FD_SET(sPeticio,&conjunt);
	int descmax = sPeticio;

	if (select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
		perror("Error en select\n");
		exit(-1);
	}

	if (FD_ISSET(0,&conjunt)) {
		printf("Entra la IP:\n");
		char ipserv[50];
		int n = read(0,ipserv,50);
		ipserv[n-1] = '\0';

		printf("Entra el Port:\n");
		int port;
		scanf("%d", &port);
	
		int sConnectat = TCP_CreaSockClient("0.0.0.0",0);

		adrr.sin_family= AF_INET;
		adrr.sin_addr.s_addr= inet_addr(ipserv);
		adrr.sin_port = htons(port);
		int i;
		for(i=0; i<8; i++) {
			adrr.sin_zero[i]='0';
		}

		if(connect(sConnectat, (struct sockaddr *)&adrr, sizeof(adrr)) == -1) {
			perror("Error en connect\n");
			exit(-1);
		}
	}

	if (FD_ISSET(sPeticio,&conjunt)) {
		adrr.sin_family= AF_INET;
		adrr.sin_addr.s_addr= inet_addr("0.0.0.0");
		adrr.sin_port = htons(port);
		int i;
		for(i=0; i<8; i++) {
			adrr.sin_zero[i]='0';
		}
		long_adrr = sizeof(adrr);
		if ((sConnectat = accept(sPeticio,(struct sockaddr*)&adrr, &long_adrr)) == -1) {
			perror("Error en accept\n");
			close(sPeticio);
			exit(-1);
		}
		printf("Connexio acceptada\n");
	}
	close(sPeticio);
	
	char buff[200];
	FD_ZERO(&conjunt);

	printf("Escriu els missatges, no ens fem responsables legals en el cas que no arribin a la destinació:\n");
	do {
		FD_SET(0,&conjunt);
		FD_SET(sConnectat,&conjunt);
		descmax = sConnectat;
		if (select(descmax+1, &conjunt, NULL, NULL, NULL) == -1) {
			perror("Error en select\n");
			exit(-1);
		}
		if (FD_ISSET(0,&conjunt)) {
			printf("Ha arribat alguna cosa per teclat\n");
			int n = read(0,buff, 200);
			buff[n-1] = '\0';
			write(sConnectat,buff,n);
		}
		if (FD_ISSET(sConnectat,&conjunt)) {
			printf("Ha arribat alguna cosa pel socketid = %d\n", sConnectat);
			int n = read(sConnectat,buff,200);
			printf("%s\n", buff);
		}

	} while (strcmp(buff, "FI") != 0);

	close(sConnectat);

	printf("Fi de la comunicació\n");

	return 0;
}
