/**************************************************************************/
/*                                                                        */
/* P1 - MI amb sockets TCP/IP - Part I                                    */
/* Fitxer p2p.c que implementa la interfície aplicació-usuari             */
/* (fa crides a la interfície de la capa MI (fitxers mi.c i mi.h)         */
/* Autors: Roger Bassons, Miquel Farreras                                 */
/*                                                                        */
/**************************************************************************/

/* Inclusió de llibreries, p.e. #include <stdio.h> o #include "meu.h"     */
/* Incloem "MIp1v4-mi.h" per poder fer crides a la interfície de MI       */
#include "MIp1v4-mi.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
/* Definició de constants, p.e., #define MAX_LINIA 150                    */


int main(int argc,char *argv[])
{
	/* Declaració de variables, p.e., int n;                          */
	int port;
	char NicLoc[300], NicRem[300];
	char IPloc[16], IPrem[16];
	int portTCPloc, portTCPrem;
	char buff[300];
	int n;
	int sck, sConnectat, sPeticio;
	int acabar;

	/* Expressions, estructures de control, crides a funcions, etc.   */
	printf("Entra el port TCP del servidor, si vols que s'assigni automàticament entra 0\n");
	scanf("%d", &port);

	if ((sPeticio = MI_IniciaEscPetiRemConv(port)) == -1) {
		return -1;
	}

	if (MI_TrobaPortEscoltaLocal(sPeticio, &port) == -1) {
		return -1;
	}
	
	printf("El port TCP del servidor es: %i\n", port);
	printf("Llistat d'Interficies amb la seva IP escoltant:\n");
	MI_MostrarIPs();

	printf("Entra el teu Nickname:\n");
	n = read(0,NicLoc,300);
	NicLoc[n-1] = '\0';

	acabar = 0;

	do {
		printf("Entra la ip per a connectar-te, FI per acabar, altrament espera\n");

		if ((sck = MI_HaArribatPetiConv(sPeticio)) == -1) {
			return -1;
		}

		if (sck == 0) {
			n = read(0,IPrem,16);
			IPrem[n-1] = '\0';
			if (strcmp(IPrem,"FI") == 0) {
				acabar = 1;
			}
			else {
				printf("Entra el Port:\n");
				scanf("%d", &portTCPrem);

				if((sConnectat = MI_DemanaConv(IPrem, portTCPrem, IPloc, &portTCPloc, NicLoc, NicRem)) == -1) {
					return -1;
				}
			}

		} else if (sck == sPeticio) {
			if((sConnectat = MI_AcceptaConv(sPeticio, IPrem, &portTCPrem, IPloc, &portTCPloc, NicLoc, NicRem)) == -1) {
				return -1;
			}
		}

		if (acabar == 0) {
			printf("El teu Nickname: %s\n", NicLoc);
			printf("Adreça IP local: %s\n", IPloc);
			printf("Port TCP local: %i\n", portTCPloc);
			printf("El Nickname remot: %s\n", NicRem);
			printf("Adreça IP remota: %s\n", IPrem);
			printf("Port TCP remot: %i\n", portTCPrem);

			printf("Escriu els missatges:\n");
			do {
				sck = MI_HaArribatLinia(sConnectat);
				if (sck == -1) {
					return -1;
				} else if (sck == 0) {
					n = read(0,buff, 300);
					buff[n-1] = '\0';
					if (strcmp(buff,"FI") != 0) {
						if (MI_EnviaLinia(sConnectat, buff) == -1) {
							printf("No s'ha pogut enviar la linia");
						}
					} else {
						n = -2;
					}
				} else if (sck == sConnectat) {
					if ((n = MI_RepLinia(sConnectat, buff)) != -2) {
						printf("%s diu: %s\n",NicRem,buff);
					}
				}

			} while (n != -2);
			MI_AcabaConv(sConnectat);
			printf("Fi de la comunicació\n");
		}
	} while (acabar == 0);

	if (MI_AcabaEscPetiRemConv(sPeticio) == -1) {
		return -1;
	}

	printf("Fi de l'aplicació\n");
	return 0;
}
